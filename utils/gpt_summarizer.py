import openai
from openai import OpenAI
import time
import re
import traceback
import tiktoken


MAXTOKS = 256
PROMPT  = "I will give you a list of texts. "
PROMPT += "You will create a summary version of them in about 50 words or less, "
PROMPT += "using everyday, non-technical language. "
PROMPT += "Summarise them as a collection, not individually. "
PROMPT += "If you cannot create a summary, return \"No\". "
PROMPT += "Otherwise, return the summary without explanation or elaboration. "
PROMPT += "Here is the list of texts:\n" 
BASE_MESSAGES = [
    {"role": "system", "content": "You are a helpful assistant."},
    {"role": "user", "content": PROMPT}]
TOKENS_PER_MESSAGE = 3  #Assume gpt-4 model
TOKENS_PER_NAME = 1


class Summarizer:
    def __init__(self, model):
        self.client = OpenAI(
            api_key='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')
#        self.client = OpenAI(
#            api_key='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')
        self.model = model
        self.input_tokens = 0
        self.output_tokens = 0
        self.tiktoken_encoding = tiktoken.encoding_for_model(model)
        self.total_cost = 0
        self.backoff = Backoff()
        
           
    def get_summary_and_title(self, texts, reporter=None):
        interrupted_report = False
        l = 1
        toks = self.__num_str_tokens__(texts[0])
        while toks <= MAXTOKS and l < len(texts):
            toks += self.__num_str_tokens__(texts[l])
            l += 1 
        messages = BASE_MESSAGES + [{
                "role": "user", "content": '\n'.join(texts[:l])}]
        self.backoff.reset()
        while True:
            try:
                completion = self.client.chat.completions.create(
                    model=self.model,
                    messages=messages,
                    temperature=0,
                    frequency_penalty=1.0,
                    presence_penalty=-1.0,
                    max_tokens=100)
                summary = self.__clean_summary__(
                    completion.choices[0].message.content)
                finish_reason = completion.choices[0].finish_reason
                self.__costs__(messages, completion.usage, 'SUMMARY')
                if finish_reason != 'stop' or self.__nulsum__(summary): 
                    return (None, None, l)
                else: 
                    self.backoff.reset()
                    break
            except KeyboardInterrupt:
                raise
            except openai.RateLimitError:
                reporter.report('Open AI rate limit')
                interrupted_report = True
                time.sleep(self.backoff.time())
            except openai.OpenAIError as oaiex:
                reporter.report('Open AI error: ' + str(oaiex))
                interrupted_report = True
                time.sleep(120)
            except BaseException as ex: 
                reporter.report('Exception: ' + str(ex))
                interrupted_report = True
                #traceback.print_exc()
                time.sleep(300)
        if interrupted_report: 
            reporter.resume_report()
            interrupted_report = False

        while True:
            try:                
                messages.append({
                        "role": "assistant", 
                        "content": summary,
                        "role": "user", 
                        "content": "Now give me a short title for the summary."})
                completion = self.client.chat.completions.create(
                    model=self.model,
                    messages=messages,
                    temperature=0,
                    frequency_penalty=1.0,
                    presence_penalty=-1.0,
                    max_tokens=25)
                title = self.__clean_title__(
                    completion.choices[0].message.content)
                finish_reason = completion.choices[0].finish_reason
                self.__costs__(messages, completion.usage, 'TITLE')
                return (summary, title, l)
            except KeyboardInterrupt:
                raise
            except openai.RateLimitError:
                reporter.report('Open AI rate limit')
                interrupted_report = True
                time.sleep(self.backoff.time())
            except openai.OpenAIError as oaiex:
                reporter.report('Open AI error: ' + str(oaiex))
                interrupted_report = True
                time.sleep(120)
            except BaseException as ex: 
                reporter.report('Exception: ' + str(ex))
                interrupted_report = True
                #traceback.print_exc()
                time.sleep(300)
                
        if interrupted_report: 
            reporter.resume_report()
            interrupted_report = False


    def __nulsum__(self, text):
        return text == 'No'
    
    
    def __clean_summary__(self, summary):
        s = summary.strip()
        if s[0] == '"' and s[-1] == '"':
            return s[1:-1]
        else: return s
    
    
    def __clean_title__(self, title):
        return re.sub('"', '', title).strip()
        
    
    def __costs__(self, messages, usage, reason):
        calculated_input_tokens = self.__num_msg_tokens__(
                messages)
        prompt_tokens = usage.prompt_tokens
        completion_tokens = usage.completion_tokens
        cost = prompt_tokens*0.00003 + (
                completion_tokens*0.00006)
        self.total_cost += cost


    def __num_str_tokens__(self, text):
        return TOKENS_PER_MESSAGE + len(
                self.tiktoken_encoding.encode(text)) + 3

    def __num_msg_tokens__(self, messages):
        num_tokens = 0
        for message in messages:
            num_tokens += TOKENS_PER_MESSAGE
            for (key, value) in message.items():
                num_tokens += len(self.tiktoken_encoding.encode(
                        value))
                if key == 'name': num_tokens += TOKENS_PER_NAME
        num_tokens += 3
        return num_tokens
    
    
class Backoff:
    def __init__(self):
        self.backoff_time = None
        self.reset()
        
        
    def reset(self):
        self.backoff_time = 60
        
        
    def time(self):
        backoff_time = self.backoff_time
        if backoff_time <= 60*60*12:
            self.backoff_time = backoff_time*2
        return backoff_time
    
        
        