import requests


def get_html(url):
    '''
    Get the content of an HTML file.
    
    Parameters:
      * url(str): the file's URL.
      
    Raises:
      * Exception if the file HTML cannot be retrieved,
        with a message giving the HTTP status code and reason
    
    Returns:
      * A string containing the HTML content of the file.
    '''

    session = requests.session()
    r = session.get(url, timeout=5)
    if r.status_code == 200: return r.text
    else: raise Exception('COULD NOT LOAD ' + url + ', status ' + str(r.status_code))
