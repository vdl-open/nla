import openai
from openai import OpenAI


class Responder():
    def __init__(self, model):
        self.client = OpenAI(
            api_key='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')
#        self.client = OpenAI(
#            api_key='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')
        self.model = model
    
    
    def generate_response(self, topic, prompt, matches):
        messages = [
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": prompt},
            {"role": "user", "content": "The topic is: " + topic},
            {"role": "user", "content": "The texts are:\n" + matches}
        ]
        completion = self.client.chat.completions.create(
                model=self.model,
                messages=messages,
                temperature=0)
        return completion.choices[0].message.content
    
