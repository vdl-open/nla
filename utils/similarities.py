import numpy as np
from numpy import dot
from numpy.linalg import norm


class Similarities:
    
    def __init__(self, vectors_are_normalized):
        self.vectors_are_normalized = vectors_are_normalized


    def similarity(self, a, b):
        '''
        Get the cosine similarity of two vectors. Round up any calculated
        values less than -1, and round down any calculated values greater
        than 1. Use the dot product, because it is the same as, and quicker 
        to calculate than, the cosine similarity, if the vectors are
        normalized.
        
        Tests indicate that converting the embeddings to tensors so that
        torch methods can be used for dot and norm marginally improves
        the time taken to calculate embeddings, but at least doubles the time
        taken to calculate similarities.
    
        Parameters: 
              * a: a numpy array-like object
              * b: a numpy array-like object
          
        Returns: the cosine similarity between a and b, as a float
        '''
        
        if self.vectors_are_normalized: sim = dot(a, b)
        else: sim = dot(a, b)/(norm(a)*norm(b))
        if sim >= -1.0:
            if sim < 1.0: return sim
            else: return 1.0
        else: return -1.0

    def difference(self, a, b, epsilon=0.000001):
        diff = a - b
        if self.vectors_are_normalized:
            norm_diff = norm(diff)
            if norm_diff < epsilon: return 0
            else: return diff/norm_diff
        else: return diff
        
        
    def centroid(self, vectors):
        total_vector = sum(vectors)
        return total_vector/norm(total_vector)
    
    
    def coherence(self, vectors):
        if len(vectors) == 0: return 1.0
        central_vector = self.centroid(vectors)
        var = sum([(1-self.similarity(
            vector, central_vector)**2) 
            for vector in vectors])/len(vectors)
        return (1-var)**0.5
    
    
    def dev_from(self, vectors, central_vector):
        return (sum([(1-self.similarity(
            vector, central_vector)**2) 
            for vector in vectors])/len(vectors))**0.5
            
            
    def dev(self, vectors):
        return self.dev_from(
            vectors, self.centroid(vectors))
          
    
    def spread(self, vectors, central_vector):
        return max([(1-self.similarity(
            v, central_vector)**2)**0.5
            for v in vectors])