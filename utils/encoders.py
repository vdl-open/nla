from sentence_transformers import SentenceTransformer


class Encoder:
    
    def __init__(self, model_path, vectors_are_normalized):
        self.model = SentenceTransformer(model_path)
        self.vectors_are_normalized = vectors_are_normalized

    def encode(self, text):
        return self.model.encode(text, convert_to_tensor=False, 
                normalize_embeddings=self.vectors_are_normalized)

