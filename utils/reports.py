import time
import traceback

from vdlpy import vdl


nlp_metadata_source_nr = -1008

class Reporter:
    def __init__(self, vdl_client, credential_and_key, task_item):
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
        self.task_item = task_item
        self.reported_action_times_dict = {}
        self.current_status = 'Nothing reported'
        self.interrupt_time = 0


    def report(self, status):
        try:
            self.__make_report__(status)
            self.current_status = status
        except: 
            pass


    def report_exception(self):
        self.__make_report__(traceback.format_exc())
        

    def interrupt_report(self, status):
        self.interrupt_time = time.time()
        try:
            self.__make_report__(status)
        except: 
            pass


    def resume_report(self):
        interrupted_minutes = int(
            (time.time() - self.interrupt_time)/60)
        try:
            self.__make_report__(
                self.current_status + 
                'INTERRUPTED FOR ' + 
                interrupted_minutes + 'MINUTES')
        except: 
            pass


    def __make_report__(self, status):
        try:
            self.vdl_client.update(
                vdl.SetUniqueObject(
                    self.task_item, 
                    vdl.NamedItm(nlp_metadata_source_nr, 
                                 'Task Has Progress state'),
                    status), 
                self.credential_and_key)
        except: 
            #traceback.print_exc()
            raise

    
    def start_progress_reporting(self, action_name):
        self.reported_action_times_dict[action_name] = time.time()
        self.report(action_name)
        
    
    def prepare_progress_reporting(self, action_name):
        self.reported_action_times_dict[action_name] = time.time()
          
          
    def progress(self, action_name, fraction_complete):
        last_report_time = self.reported_action_times_dict[action_name]
        current_time = time.time()
        i = 0
        if current_time - last_report_time > 30:
            i += 1
            self.report(action_name + ' ' + str(int(
                                    fraction_complete*100)) + '% complete')
            self.reported_action_times_dict[action_name] = current_time
    
    
    def end_progress_reporting(self, action_name):
        self.report(action_name + ' completed')
        self.reported_action_times_dict.pop(action_name)
    
    
    def cancel_progress_reporting(self, action_name):
        self.reported_action_times_dict.pop(action_name)
        