from vdlpy import vdl

import reports
import traceback


class BatchUpdater:
    def __init__(self, vdl_client, credential_and_key, reporter):
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
        self.reporter = reporter
               

    def make_changes(self, change_batches, action_name):
        if self.reporter is not None:
            self.reporter.prepare_progress_reporting(action_name)
        change_batch_ix = 0
        stored_items_dict = {}
        while change_batch_ix < len(change_batches):
            changes = change_batches[change_batch_ix]
            changes_copy = changes.copy()
            change_batch_ix += 1
            while ( change_batch_ix < len(change_batches)) and (
                    len(changes) + len(change_batches[change_batch_ix])) < 256:
                changes.extend(change_batches[change_batch_ix])
                change_batch_ix += 1
            
            for change in changes:
                self.__resolve_stored_items__(change, stored_items_dict)
                
            try:
                stored_items_dict.update(
                    self.vdl_client.update(changes, self.credential_and_key))
            except: 
                if self.reporter is not None:
                    self.reporter.cancel_progress_reporting(action_name)
                raise
            if self.reporter is not None:
                self.reporter.progress(
                        action_name, change_batch_ix/len(change_batches))
            
        if self.reporter is not None:
            self.reporter.cancel_progress_reporting(action_name)
        return stored_items_dict
    
        
    def __resolve_stored_items__(self, change, stored_items_dict):
        if isinstance(change, vdl.PutTriple):
            change.subj = self.__resolve__(change.subj, stored_items_dict)
            change.vrb = self.__resolve__(change.vrb, stored_items_dict)
            change.obj = self.__resolve__(change.obj, stored_items_dict)
            
                
    def __resolve__(self, quantum, stored_items_dict):
        if isinstance(quantum, vdl.Unknown):
            itm = stored_items_dict.get(quantum.name)
            if itm is None: return quantum
            else: return itm
        else: return quantum
