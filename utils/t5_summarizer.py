from transformers import T5Tokenizer, T5ForConditionalGeneration
import contextlib
from io import StringIO
import sys
import os
import re
import traceback


MAX_INPUT_TOKENS = 1024
SUMMARY_BASE = "You are a helpful assistant. "
SUMMARY_BASE += "I will give you a list of texts related to a topic. "
SUMMARY_BASE += "You will summarise the topic, in 50-100 words,"
SUMMARY_BASE += "using everyday, non-technical language. "
SUMMARY_BASE += "Return the summary without explanation or elaboration. "
SUMMARY_BASE += "Here is the list of texts:\n" 


class Summarizer:
    def __init__(self, tokenizer_path, model_path):
        self.t5tokenizer = T5Tokenizer.from_pretrained(tokenizer_path, max_new_tokens=2048)        
        #("/Users/chrisharding/nlp/model-tokenizers/google/flan-t5-large", max_new_tokens=512)
        # Suppress warning if there are > 512 tokens. We will check the number of tokens and not present more than 512 to the model.
        self.t5tokenizer.model_max_length = sys.maxsize
        self.t5model = T5ForConditionalGeneration.from_pretrained(model_path)
        #("/Users/chrisharding/nlp/models/google/flan-t5-large", resume_download=True)
        self.nr_base_tokens = self.__get_nr_base_tokens__()
           
           
    def get_summary_and_title(self, texts):
        user_content = '\n'.join(texts)
        l = len(texts)
        try: 
            while True:
                text = SUMMARY_BASE + user_content
                input_tokens = self.t5tokenizer(
                    text, return_tensors="pt", add_special_tokens=True)
                nr_tokens = len(input_tokens['input_ids'][0])
                if nr_tokens <= MAX_INPUT_TOKENS or l == 1: break
                l = max(1, int(l*MAX_INPUT_TOKENS/nr_tokens))
                user_content = '\n'.join(texts[:l])
            
            min_new_tokens = min(64, int(
                    (nr_tokens - self.nr_base_tokens)/5))    
            output_tokens = self.t5model.generate(
                **input_tokens, 
                min_new_tokens = min_new_tokens,
                max_new_tokens = 128, 
                no_repeat_ngram_size = 3,
                use_cache  = False)
            
            ot = output_tokens[0]
            summary = self.t5tokenizer.decode(
                    output_tokens[0], skip_special_tokens=True, 
                    clean_up_tokenization_spaces=True)
            if summary == "No": return (None, None, l)
            
            text = 'Now please create a short title' + (
                    'for the following summary .') + summary
            input_tokens = self.t5tokenizer(
                text, return_tensors="pt", add_special_tokens=True)
            max_new_tokens = 50
            output_tokens = self.t5model.generate(
                **input_tokens, 
                max_new_tokens = max_new_tokens, 
                no_repeat_ngram_size = 3,
                use_cache  = True)
            
            title = self.t5tokenizer.decode(
                    output_tokens[0], skip_special_tokens=True, 
                    clean_up_tokenization_spaces=True)

            return (summary, title, l)
        except KeyboardInterrupt:
            raise
        except: 
            raise
           
           
    def get_summary(self, texts):
        return self.__get_sum__(texts, 256)
           
           
    def get_title(self, texts):
        return self.__get_sum__(texts, 32)
    
           
    def __get_sum__(self, texts, max_in_tokens):
        user_content = '\n'.join(texts)
        l = len(texts)
        try: 
            while True:
                text = 'Summarize:\n' + user_content
                input_tokens = self.t5tokenizer(
                    text, return_tensors="pt", add_special_tokens=True)
                nr_tokens = len(input_tokens['input_ids'][0])
                if nr_tokens <= max_in_tokens or l == 1: break
                l = max(1, int(l*max_in_tokens/nr_tokens))
                user_content = '\n'.join(texts[:l])
            
            output_tokens = self.t5model.generate(
                **input_tokens, 
                max_new_tokens=nr_tokens/3,
                no_repeat_ngram_size = 3,
                use_cache  = False)
            
            return (self.t5tokenizer.decode(
                    output_tokens[0], skip_special_tokens=True, 
                    clean_up_tokenization_spaces=True), l)
            

            return (summary, title, l)
        except KeyboardInterrupt:
            raise
        except: 
            raise
            
            
    def __get_nr_base_tokens__(self):
        input_tokens = self.t5tokenizer(
                SUMMARY_BASE, return_tensors="pt", add_special_tokens=True)
        return len(input_tokens['input_ids'][0])

