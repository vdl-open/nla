from vdlpy import vdl
from utils import changes
from utils import reports
import vdl_parsed_file_sets


NLP_METADATA_SOURCE_NR = -1008
'''
The numeric identifier of the virtual data lake source containing
metadata for natural language processing.
'''


class Store:
    def __init__(self, vdl_client, credential_and_key):
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
        
        
    def get_parsed_file_set_to_delete_id_set(self):
        parsed_file_set_x = vdl.Unknown('Parsed File Set')
        file_set_x = vdl.Unknown('File Set')                
        solutions = self.vdl_client.query(
                [(  parsed_file_set_x,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                        'Parsed file set Is parsed from File set'),
                    file_set_x)],
                self.credential_and_key)
        parsed_file_set_id_set_dict = {}
        for solution in solutions:
            file_set_id = solution['File Set'].itemid
            parsed_file_set_id = solution['Parsed File Set'].itemid
            parsed_file_set_id_set = parsed_file_set_id_set_dict.get(
                file_set_id)
            if parsed_file_set_id_set is None:
                parsed_file_set_id_set_dict[file_set_id
                                            ] = {parsed_file_set_id}
            else: parsed_file_set_id_set.add(parsed_file_set_id)

        solutions = self.vdl_client.query(
                [(  parsed_file_set_x,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Parsed file set Is Complete'),
                    True)],
                self.credential_and_key)
        complete_parsed_file_set_id_set = {
                solution['Parsed File Set'].itemid
                    for solution in solutions}
        
        parsed_file_set_to_delete_id_set = set()
        parsed_file_set_to_keep_id_set = set()        
        for (file_set_id, parsed_file_set_id_set
             ) in parsed_file_set_id_set_dict.items():
            file_set_parsed_file_set_id = None
            for parsed_file_set_id in parsed_file_set_id_set:
                if file_set_parsed_file_set_id is None and (
                        parsed_file_set_id in (
                            complete_parsed_file_set_id_set)):
                    parsed_file_set_to_keep_id_set.add(
                        parsed_file_set_id)
                    file_set_parsed_file_set_id = parsed_file_set_id
                else: parsed_file_set_to_delete_id_set.add(
                        parsed_file_set_id)
        parsed_file_set_to_delete_id_set.update(
            complete_parsed_file_set_id_set.difference(
                parsed_file_set_to_keep_id_set))
        return parsed_file_set_to_delete_id_set


    # Get the set of item identifiers of complete file sets
    def get_complete_file_set_id_set(self):
        solutions = self.vdl_client.query(
                [(  vdl.Unknown('File Set'),
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'File set Is Complete'),
                    True)],
                self.credential_and_key)
        return {solution['File Set'].itemid 
                for solution in solutions}


    # Get the set of item identifiers of parsed file sets 
    # that are complete
    def get_complete_parsed_file_set_id_set(self):
        parsed_file_set_x = vdl.Unknown('Parsed File Set')
        solutions = self.vdl_client.query(
                [(  parsed_file_set_x, 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Parsed file set Is Complete'), 
                    True)],
                self.credential_and_key)
        return {solution['Parsed File Set'].itemid 
                for solution in solutions}
        
    
    def get_complete_file_set_id_set(self):
        file_set_x = vdl.Unknown('File Set')
        solutions = self.vdl_client.query(
                [(  file_set_x, 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'File set Is Complete'), 
                    True)],
                self.credential_and_key)
        return {solution['File Set'].itemid 
                for solution in solutions}


    # Get the set of item identifiers of file sets that have
    # been parsed to create complete parsed file sets
    def get_completely_parsed_file_set_id_set(self):
        file_set_x = vdl.Unknown('File Set')
        parsed_file_set_x = vdl.Unknown('Parsed File Set')
        solutions = self.vdl_client.query(
                [(  parsed_file_set_x,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Parsed file set Is parsed from File set'),
                    file_set_x),
                 (  parsed_file_set_x, 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Parsed file set Is Complete'), 
                    True)],
                self.credential_and_key)
        return {solution['File Set'].itemid 
                for solution in solutions}


    def get_source_nr_and_access_level(self, file_set_id):
        file_set_item = vdl.Itm(file_set_id)
        solutions = self.vdl_client.query(
                [(file_set_item,
                  vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'File set Has Analysis source'),
                  vdl.Unknown('Source')),
                 (file_set_item,
                  vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'File set Has Analysis read level'),
                  vdl.Unknown('Read Level'))],
                self.credential_and_key)
        if len(solutions) == 0: return (None, None)
        elif len(solutions) == 1: return (
            solutions[0]['Source'], solutions[0]['Read Level'])
        else: raise Exception(
            'File set has multiple analysis source numbers ' + (
            'and read levels: ' + file_set_id))
        
        
    def new_parsed_file_set(
            self, file_set_item, source_nr, read_level):
        handle = 'Parsed File Set'
        items_dict = self.vdl_client.update(
            [ vdl.CreateItem(handle,
                             source_nr, read_level),
              vdl.PutTriple( vdl.Unknown(handle),
                             vdl.NamedItm(
                                NLP_METADATA_SOURCE_NR,
                                'Parsed file set Is parsed from File set'),
                             file_set_item)
            ], self.credential_and_key)
        return items_dict[handle]


    def get_parsed_file_set(self, file_set_id):
        file_set_item = vdl.Itm(file_set_id)
        parsed_file_set_item = self.__get_parsed_file_set_item__(
                file_set_item) 
        if parsed_file_set_item is None: return None
        vdl_parsed_file_set = vdl_parsed_file_sets.VdlParsedFileSet(
                parsed_file_set_item, file_set_item,
                self.vdl_client, self.credential_and_key)
        return vdl_parsed_file_set.get_parsed_file_set()


    def store_sections(
            self, section_with_nested_subsections, 
            parsed_file_item, source_nr, read_level, 
            reporter):
        '''
        Store a section and its recursively nested subsections and their
        sentences as a parsed file in a virtual data lake, and note that
        the parsed file is loaded.
    
        Parameters:
          * section_with_nested_subsections: a a section with its 
            recursively nested subsections.
          * parsed_file_item(vdl.Itm): the item representing the
            parsed file whose sections are to be stored
          * reporter (reports.Reporter) the reporter to be used to
            report progress
        '''
        
        change_batches = []
        if section_with_nested_subsections.title is not None:
            change_batches.append(
                    [vdl.SetUniqueObject(
                        parsed_file_item,
                        vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                     'Parsed file Has Title'), 
                        section_with_nested_subsections.title)])
        self.__add_section_changes__(
                change_batches, parsed_file_item, 
                section_with_nested_subsections, 'Section0',
                source_nr, read_level)
        changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, 
                reporter).make_changes(
                        change_batches, 'Storing parsed file sections')


    def note_complete(self, parsed_file_set_item):
        self.vdl_client.update(
            [vdl.SetUniqueObject(
                parsed_file_set_item,
                vdl.NamedItm(NLP_METADATA_SOURCE_NR,
                             'Parsed file set Is Complete'),
                True)],
            self.credential_and_key)
        
        
    def remove_parsed_file_set_by_name(
            self, parsed_file_set_name):
        parsed_file_set_item = self.__get_parsed_file_set__(
                parsed_file_set_name)
        self.remove_parsed_file_set(parsed_file_set_item)
        
        
    def remove_parsed_file_set(self, parsed_file_set_item):
        if parsed_file_set_item is None: return
        reporter = reports.Reporter(
                self.vdl_client, self.credential_and_key, 
                parsed_file_set_item)
        change_batches = []
        parsed_file_items = self.__get_parsed_file_set_file_items__(
                parsed_file_set_item)
        for parsed_file_item in parsed_file_items:
            self.__add_remove_parsed_file_changes__(
                    change_batches, parsed_file_item)
            
        changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, reporter
                    ).make_changes(
                        change_batches, 
                        'Removing parsed file set')

        self.vdl_client.update(
            [vdl.DeleteItem(parsed_file_set_item)],
            self.credential_and_key)
        
        
    def new_parsed_file(self, file_item, source_nr, read_level):
        handle = 'Parsed File'
        items_dict = self.vdl_client.update(
            [ vdl.CreateItem(
                    handle, source_nr, read_level),
              vdl.PutTriple( vdl.Unknown(handle),
                             vdl.NamedItm(
                                NLP_METADATA_SOURCE_NR,
                                'Parsed file Is parsed from File'),
                             file_item)
            ], self.credential_and_key)
        return items_dict[handle]
        
    
    def remove_parsed_file(self, parsed_file_item):  
        change_batches = []
        self.__add_remove_parsed_file_changes__(
                change_batches, parsed_file_item)
        changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, None
                    ).make_changes(
                        change_batches, 'Removing parsed file')
        
          
    def get_file_items_and_urls(self, file_set_item):
        file_x = vdl.Unknown('File')
        solutions = self.vdl_client.query(
            [(  file_x, 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'File Is in File set'), 
                file_set_item),
             (  file_x, 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Html file Has Url'), 
                vdl.Unknown('Url'))], 
            self.credential_and_key)
        return [(solution['File'], solution['Url']) 
                for solution in solutions]
                
        
    def __get_parsed_file_set__(self, file_set_name):
        file_set_x = vdl.Unknown('File Set')
        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Parsed File Set'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Parsed file set Is parsed from File set'),
                file_set_x),
             (  file_set_x,
                vdl.NamedItm(
                   NLP_METADATA_SOURCE_NR, 'File set Has Name'),
                file_set_name)],
            self.credential_and_key)
        for solution in solutions:
            return solution['Parsed File Set']
   
    
    def __get_parsed_file_set_item__(self, file_set_item):                
        solutions = self.vdl_client.query(
                [(  vdl.Unknown('Parsed File Set'),
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Parsed file set Is parsed from File set'),
                    file_set_item)],
                self.credential_and_key)
        if len(solutions) == 0: return None
        if len(solutions) > 1: raise Exception(
                'File set has multiple parsed files')
        return solutions[0]['Parsed File Set']        
    
    
    def __get_file_set_item__(self, parsed_file_set_item):
        solutions = self.vdl_client.query(
                [(  parsed_file_set_item,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Parsed file set Is parsed from File set'),
                    vdl.Unknown('File Set'))],
                self.credential_and_key)
        if len(solutions) == 0: return None
        if len(solutions) > 1: raise Exception(
            'Parsed file set is parsed from multiple file sets')
        return solutions[0]['File Set']        
        
    
    def __get_parsed_file_set_file_items__(self, parsed_file_set_item):
        file_set_x = vdl.Unknown('File Set')
        file_x = vdl.Unknown('File')
        solutions = self.vdl_client.query(
                [(  parsed_file_set_item, 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Parsed file set Is parsed from File set'), 
                    file_set_x),
                 (  file_x, 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'File Is in File set'), 
                    file_set_x),
                 (  vdl.Unknown('Parsed File'), 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Parsed file Is parsed from File'), 
                    file_x)],
                self.credential_and_key)
        doc_items = [solution['Parsed File'] for solution in solutions]
        return doc_items
   
    
    def __add_section_changes__(
            self, change_batches, parsed_file_item, 
            section, section_handle, source_nr, read_level):
        '''
        Add to a list of lists of changes lists of the changes 
        to store a section with its title and sentences
    
        Parameters:
         * change_batches: the list of lists of changes to be added to
         * parsed_file_item (vdl.Itm): the item representing the parsed file
           containing the section
         * section(`docs.Section`): the section to be stored
         * section_handle(str): the handle to be used to represent
           the section in the changes
         * source_nr the source number of the source where new items
           are to be created
         * read_level the read access level of new items
        '''
    
        section_x = vdl.Unknown(section_handle)
        changes = []
        changes.append(vdl.CreateItem(
            section_x, source_nr, read_level))
        changes.append(vdl.PutTriple(
                parsed_file_item, 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Parsed file Has Section'), 
                section_x))
        if section.title is not None:
            changes.append(vdl.PutTriple(
                section_x, 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Section Has Title'), 
                section.title))
        i = 0;
        for sentence in section.sentences:
            if sentence.complexity > 10:
                sentence_x = vdl.Unknown(section_handle + '-' + str(i))
                changes.append(vdl.CreateItem(
                        sentence_x, source_nr, read_level))
                changes.append(vdl.PutTriple(
                    section_x, 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Section Has Sentence'), 
                    sentence_x))
                changes.append(vdl.PutTriple(
                    sentence_x, 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Sentence Has Text'), 
                    sentence.text))
                i += 1
        change_batches.append(changes)
        
        j = 0
        for subsection in section.subsections:
            subsection_handle = section_handle + '.' + str(j)
            self.__add_section_changes__(
                    change_batches, parsed_file_item, subsection, 
                    section_handle + '.' + str(j),
                    source_nr, read_level)
            change_batches.append([vdl.PutTriple(
                section_x, 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Section Has Subsection'), 
                vdl.Unknown(subsection_handle))])
            j += 1
        
        
    def __add_remove_parsed_file_changes__(
            self, change_batches, parsed_file_item):
        section_x = vdl.Unknown('Section')
        solutions = self.vdl_client.query(
                [(parsed_file_item, 
                  vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                               'Parsed file Has Section'), 
                  section_x),
                 (section_x, 
                  vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                               'Section Has Sentence'), 
                  vdl.Unknown('Sentence'))], 
                self.credential_and_key)
        section_id_set = {solution['Section'].itemid 
                          for solution in solutions}
        sentence_id_set = {solution['Sentence'].itemid 
                           for solution in solutions}
    
        solutions = self.vdl_client.query(
                [(parsed_file_item, 
                  vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                               'Parsed file Has Section'), 
                  section_x)], 
                self.credential_and_key)
        for solution in solutions:
            section_id_set.add(solution['Section'].itemid)
        
        for sentence_id in sentence_id_set:
            change_batches.append([vdl.DeleteItem(vdl.Itm(sentence_id))])
        for section_id in section_id_set:
            change_batches.append([vdl.DeleteItem(vdl.Itm(section_id))])
        change_batches.append([vdl.DeleteItem(parsed_file_item)])
