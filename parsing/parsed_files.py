'''
A set of files that have been parsed to find their
sections, subsections and sentences. Each file has
a head section. This is a conceptual section that
contains all of its content. All of its real sections
are (recursively) subsections of its head section. 
Each of its sentences is contained in one, and only
one, section, which may be its head section.

Each file, section and sentence is represented by a 
virtual data lake item. The identifiers of these
items are the keys of the parsed files, sections and
sentences dictionaries.
'''


class ParsedFileSet:        
    def __init__(self, file_set_name):
        self.file_set_name = file_set_name
        self.parsed_files_dict = {}
        self.sections_dict = {}
        self.sentences_dict = {}
        
        
    def get_name(self):
        return self.file_set_name


class ParsedFile:
    def __init__(self, title, file_url):
        self.title = title
        self.file_url = file_url
        self.section_id_set = set()        
        
        
class Section:
    def __init__(self, title):
        self.title = title
        self.sentence_id_set = set()


class Sentence:
    def __init__(self, text):
        self.text = text
