from vdlpy import vdl
import urllib


SYS_SOURCE_NR = -1
NLP_METADATA_SOURCE_NR = -1008


class Store:
    def __init__(self, vdl_client, credential_and_key):
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
        
        
    def get_conversion_id_set(self):
        solutions = self.vdl_client.query(
                [(  vdl.Unknown('Conversion'),
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Word to html conversion Converts to Path'),
                    vdl.Unknown('Path'))],
                self.credential_and_key)
        return {solution['Conversion'].itemid 
                for solution in solutions}
        
        
    def get_completed_conversion_id_set(self):
        solutions = self.vdl_client.query(
                [(  vdl.Unknown('Conversion'),
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Word to html conversion Is Complete'),
                    True)],
                self.credential_and_key)
        return {solution['Conversion'].itemid 
                for solution in solutions}
        
        
    def get_conversion_spec(
            self, conversion_item):
        solutions = self.vdl_client.query(
                [(  conversion_item,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Word to html conversion Converts Word file'),
                    vdl.Unknown('Bin')),
                 (  conversion_item,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Word to html conversion Converts to Path'),
                    vdl.Unknown('Path')),
                 (  conversion_item,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Word to html conversion Has Read level'),
                    vdl.Unknown('Level'))],
                self.credential_and_key)
        if len(solutions) == 0:
            raise Exception(
                'Conversion path and access level not found: ' + (
                conversion_item.itemid))
        elif len(solutions) > 1:
            raise Exception(
                'Multiple conversion paths and access levels found: ' + (
                conversion_item.itemid))
        style_map_path_string = self.__get_style_map_path_string__(
                conversion_item)
        return ConversionSpec(
            solutions[0]['Bin'], 
            solutions[0]['Path'], 
            solutions[0]['Level'],
            style_map_path_string)
     
     
    def get_style_map(self, style_map_path): 
        if style_map_path is None: 
                return None
        style_map_item = style_map_path.get_item(
                self.vdl_client, self.credential_and_key)
        solutions = self.vdl_client.query(
                [(  style_map_item,
                    vdl.NamedItm(SYS_SOURCE_NR, 
                                 'fileHasContent'),
                    vdl.Unknown('Map'))],
                self.credential_and_key)
        if len(solutions) == 0:
            raise Exception(
                'Style map not found: ' + str(
                    style_map_path))
        elif len(solutions) > 1:
            raise Exception(
                'Multiple style maps found: ' + str(
                    style_map_path))
        else: return solutions[0]['Map']
        
        
    def store_html(
            self, html_file_path, access_level, html_text):
        html_file_path.store_file(
                access_level, html_text,
                self.vdl_client, self.credential_and_key)
              
            
    def note_complete(self, conversion_item):
        self.vdl_client.update(
            [vdl.SetUniqueObject(
                conversion_item,
                vdl.NamedItm(NLP_METADATA_SOURCE_NR,
                             'Word to html conversion Is Complete'),
                True)],
            self.credential_and_key)
        
        
    def __get_style_map_path_string__(
            self, conversion_item):
        solutions = self.vdl_client.query(
                [(  conversion_item,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Word to html conversion Has Style map path'),
                    vdl.Unknown('Path'))],
                self.credential_and_key)
        if len(solutions) == 0: return None
        elif len(solutions) > 1:
            raise Exception(
                'Multiple file map paths found: ' + (
                conversion_item.itemid))
        return solutions[0]['Path']

 
class ConversionSpec:
    def __init__(self, word_bin, file_path_string, 
                 access_level, style_map_path_string):    
        self.word_bin = word_bin
        self.file_path = Path(file_path_string)
        self.access_level = access_level
        if style_map_path_string is None:
            self.style_map_path = None
        else: self.style_map_path = Path(style_map_path_string)
        
    
class Path:
    def __init__(self, path_string):
        path_segments = urllib.parse.unquote(path_string
                                             ).split('/')
        l = len(path_segments)
        if l < 3:
            raise Exception('Malformed path: ' + path_string)
        self.source = path_segments[0]
        self.name = path_segments[l-1]
        self.workspace = '/'.join(path_segments[1:l-1])
        self.source_nr = None
        self.workspace_item = None
        self.member_item = None
    
    
    def store_file(self, access_level, html_text,
                   vdl_client, credential_and_key):
        if self.get_item(
                vdl_client, credential_and_key) is None:
            self.__create_file__(access_level, html_text,
                   vdl_client, credential_and_key)
        else: self.__replace_file__(access_level, html_text,
            vdl_client, credential_and_key)
        
        
    def get_item(self, vdl_client, credential_and_key):
        if self.member_item is None:
            workspace_item = self.get_workspace_item(
                vdl_client, credential_and_key)
            member_x = vdl.Unknown('Member')
            solutions = vdl_client.query(
                [(  workspace_item,
                    vdl.NamedItm(SYS_SOURCE_NR, 
                                 'workspaceHasMember'),
                    member_x),
                 (  member_x,
                    vdl.NamedItm(SYS_SOURCE_NR, 
                                 'workspaceMemberHasName'),
                    self.name)],
                credential_and_key)
            if len(solutions) == 0: return None
            elif len(solutions) > 1:
                raise Exception(
                    'Multiple workspace members found: ' + (
                    self.name))
            self.member_item = solutions[0]['Member']
        return self.member_item
        
            
    def get_workspace_item(
            self, vdl_client, credential_and_key):
        if self.workspace_item is None:
            source_nr = self.get_source_nr(
                vdl_client, credential_and_key)
            workspace_x = vdl.Unknown('WS')
            solutions = vdl_client.query(
                [(  workspace_x,
                    vdl.NamedItm(SYS_SOURCE_NR, 
                                 'workspaceHasName'),
                    self.workspace)],
                credential_and_key)
            for solution in solutions:
                ws_item = solution['WS']
                if __get_item_source_nr__(ws_item
                                     ) == source_nr: 
                    self.workspace_item = ws_item
            if self.workspace_item is None:
                raise Exception(
                    'Workspace not found: ' + (
                    self.source + ':' + self.workspace))
        return self.workspace_item


    def get_source_nr(self, vdl_client, credential_and_key):
        if self.source_nr is None:
            source_x = vdl.Unknown('SOURCE')
            solutions = vdl_client.query(
                [(  source_x,
                    vdl.NamedItm(SYS_SOURCE_NR, 
                                 'sourceHasName'),
                    self.source),
                 (  source_x,
                    vdl.NamedItm(SYS_SOURCE_NR, 
                                 'sourceHasNumber'),
                    vdl.Unknown('Nr'))],
                credential_and_key)
            if len(solutions) == 0:
                raise Exception(
                    'Source number not found: ' + self.source)
            elif len(solutions) > 1:
                raise Exception(
                    'Multiple source numbers found: ' + self.source)
            self.source_nr = solutions[0]['Nr']
        return self.source_nr

        
    def __create_file__(
            self, access_level, html_text,
            vdl_client, credential_and_key):
        source_nr = self.get_source_nr(
                vdl_client, credential_and_key)
        workspace_item = self.get_workspace_item(
                vdl_client, credential_and_key)
        file_unknown = vdl.Unknown('File')
        vdl_client.update(
            [ vdl.CreateItem('File', source_nr, access_level),
              vdl.PutTriple( workspace_item,
                             vdl.NamedItm(
                                SYS_SOURCE_NR,
                                'workspaceHasMember'),
                             file_unknown),
              vdl.PutTriple( file_unknown,
                             vdl.NamedItm(
                                SYS_SOURCE_NR,
                                'workspaceMemberHasName'),
                             self.name),
              vdl.PutTriple( file_unknown,
                             vdl.NamedItm(
                                SYS_SOURCE_NR,
                                'workspaceMemberHasType'),
                             'File'),
              vdl.PutTriple( file_unknown,
                             vdl.NamedItm(
                                SYS_SOURCE_NR,
                                'fileHasContent'),
                             html_text),
              vdl.PutTriple( file_unknown,
                             vdl.NamedItm(
                                SYS_SOURCE_NR,
                                'fileHasMimeType'),
                             'text/html')
            ], credential_and_key)
        
        
    def __replace_file__(self, access_level, html_text,
            vdl_client, credential_and_key):
        file_item = self.get_item(vdl_client, credential_and_key)
        if access_level is None: changes = []
        else: changes = [vdl.UpdateItem(
                file_item, access_level)]
        changes.extend(
            [ vdl.SetUniqueObject( 
                    file_item,
                    vdl.NamedItm(
                        SYS_SOURCE_NR,
                        'workspaceMemberHasType'),
                    'File'),
              vdl.SetUniqueObject(
                    file_item,
                    vdl.NamedItm(
                        SYS_SOURCE_NR,
                        'fileHasContent'),
                    html_text),
              vdl.SetUniqueObject( 
                    file_item,
                    vdl.NamedItm(
                        SYS_SOURCE_NR,
                        'fileHasMimeType'),
                    'text/html')
            ])
        vdl_client.update(
            changes, credential_and_key)
        
        
    def __str__(self):
        return self.source + '/'+ self.workspace + '/' + self.name 
    
    
def __get_item_source_nr__(itm):
    ix = itm.itemid.index('_')
    return int(itm.itemid[:ix])
 