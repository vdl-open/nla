'''
This module extracts sections and sentences from an HTML file. 
'''

import re

from bs4 import BeautifulSoup
from bs4 import Comment
import nltk
from nltk.tokenize import sent_tokenize

import documents


section_tags = {'h1':8, 'h2':7, 'h3':6, 'dt':5, 'h4':3, 'h5':2, 'h6':1}
''' 
HTML tags of section headings and term definitions, with their 
importance weights for topic analysis. (Definitions are considered 
in the same way as sections for the purpose of identifying topics.)
'''

significant_parts_of_speech = {'ADJ', 'ADV', 'NOUN', 'PROPN', 'VERB'}
'''
Parts of speech that are significant for analysing a sentence.
'''

title_weight = 10
''' The importance weight of the overall document.'''

inline_tags = set(['a', 'abbr', 'b', 'big', 'dfn', 'em', 'ins', 'font',
                   'mark', 'q', 'ruby', 'rt', 'small', 'strong', 'time', 
                   'tt', 'u', 'var', 'wbr'])
'''HTML tags that occur within sentences.'''

skipped_tags = set(['applet', 'area', 'audio', 'button', 'canvas', 'code', 
                    'data', 'datalist', 'del', 'img', 'input', 'kbd', 
                    'label', 'link', 'map', 'meter', 'object', 'output', 
                    'picture', 'progress', 'script', 'source', 'style',
                    'sum', 'sup', 'svg', 'textarea', 'track','video'])
'''HTML tags that are to be skipped because they do not contain document text.'''
  
  
def get_head_section_with_nested_subsections(html):
    '''
    Get the head section of an HTML document, with its recursively nested
    subsections and their sentences.
    
    Parameters:
    * html(str): an HTML document.
    
    Returns:
    * The head section of the document, with its recursively nested
      subsections and their sentences.
    '''

    soup = BeautifulSoup(html, features="html.parser")
    unnested_sections = __get_unnested_sections__(soup)
    section_with_nested_subsections = __get_section_with_nested_subsections__(
                                                                unnested_sections)
    __add_sentences__(section_with_nested_subsections)
    return section_with_nested_subsections


def __get_unnested_sections__(soup):   
    '''
    Get the sections of an HTML document, in the order that they
    would be displayed, but not nested.
    
    Parameters:
    * soup: a Beautiful Soup (version bs4) analyser, loaded with
      the HTML content of the document.
    
    Returns:
    * A list of the sections of the document.
     '''

    if soup is None: title = None
    else: 
        soup_title =  soup.title
        if soup_title is None: title = 'Untitled File'
        else:
            title_strings = list(soup_title.stripped_strings)
            if len(title_strings) == 0: title = 'Untitled File'
            else: title = ' '.join(title_strings)
    unnested_sections = [documents.Section(title, 0, title_weight)]
    if soup.body is not None:
        text = __find_unnested_sections__(
                unnested_sections, soup.body, None)
        if text is not None: unnested_sections[0].add_text(re.sub("\s+", ' ', text))
    return unnested_sections


def __find_unnested_sections__(unnested_sections, parent_tag, text):
    '''
    Add to a list of sections of an HTML document the sections
    defined by an HTML tag and its descendants.
    
    Parameters:
    * unnested_sections: a list of `Section` objects.    
    * parent_tag: a Beautiful Soup tag from the analysis of the document.
    * text(str): the text from previously processed tags that has
      not yet been added to a section.
       
    Returns:
    * The text from this and previously processed tags that has
      not yet been added to a section.
    '''

    children = parent_tag.contents
    if children is None: return text
    section_found = False
    for child in children:
        if child.name not in skipped_tags:
            if child.name is None: 
                if not isinstance(child, Comment):
                    new_text = str(child)
                    if len(new_text) > 0:
                        if text is None: text = new_text
                        else: text = text + new_text
            elif child.name in section_tags:
                section_found = True
                if text is not None:
                    unnested_sections[-1].add_text(re.sub("\s+", ' ', text))
                    text = None
                ss = child.stripped_strings
                if ss is None: title = None
                else: title = ' '.join(list(ss))
                if child.name[0] == 'h' and len(child.name) == 2 and child.name[1].isnumeric():
                    rank = int(child.name[1])
                else: rank = None
                unnested_sections.append(documents.Section(title, rank, section_tags.get(child.name)))
            elif child.name in inline_tags:
                text = __find_unnested_sections__(unnested_sections, child, text)
            else:
                if text is not None: unnested_sections[-1].add_text(re.sub("\s+", ' ', text))
                text = __find_unnested_sections__(unnested_sections, child, None)
    if section_found:
        if text is not None: 
            unnested_sections[-1].add_text(re.sub("\s+", ' ', text))
            text = None
    return text

 
def __get_section_with_nested_subsections__(unnested_sections):
    '''
    Get a section with its recursively nested subsections from
    a list of unnested sections.
    
    Parameters:
    * unnested_sections: a list of `Section` objects that have not yet
      had their subsections added.    
       
    Returns:
    * The first section in the list with its subsections added.
      The subsections are sections that appear in the list.
      Each subsection has its subsections added, recursively.
    '''

    section_stack = []
    for section in unnested_sections:
        rank = section.rank
        while len(section_stack) > 0 and (
                section_stack[-1].rank is None or 
                (rank is not None and rank <= section_stack[-1].rank)): 
            section_stack.pop()
        if len(section_stack) > 0: 
            section_stack[-1].add_subsection(section)
        section_stack.append(section)
    return section_stack[0]


def __add_sentences__(section_with_nested_subsections):
    '''
    Add to a section and its recursively nested subsections the
    sentences contained in their text, and set their title.
    
    Parameters:
    * section_with_nested_subsections: a section with its 
        recursively nested subsections.
    '''

    for text in section_with_nested_subsections.texts:
        sentences = sent_tokenize(text.strip())
        for sent in sentences:
            if len(sent.strip()) > 0:
                section_with_nested_subsections.sentences.append(
                    documents.Sentence(sent, __complexity__(sent)))
    for subsection in section_with_nested_subsections.subsections:
        __add_sentences__(subsection)


def __complexity__(sent):
    '''
    Get a measure of complexity of a sentence
            
    Parameters:
    * sent(str): the text of a sentence
    
    Returns:
    * a measure of complexity of the sentence(int) expressed as
    10 times the number of its verbs plus the number of its nouns.
    '''
    
    verbs = 0
    nouns = 0    
    words = nltk.word_tokenize(sent)
    tagged_words = nltk.pos_tag(words)
    for tagged_word in tagged_words:
        tag = tagged_word[1]
        if tag.startswith('VB'): verbs +=1
        elif tag.startswith('NN'): nouns +=1
    return 10*verbs + nouns
