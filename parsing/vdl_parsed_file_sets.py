from vdlpy import vdl
import parsed_files


nlp_metadata_source_nr = -1008
'''
The numeric identifier of the virtual data lake source containing
metadata for natural language processing.
'''
    

class VdlParsedFileSet:
    def __init__(self, parsed_file_set_item, file_set_item,
                 vdl_client, credential_and_key):
        self.parsed_file_set_item = parsed_file_set_item
        self.file_set_item = file_set_item
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
                
                
    def get_parsed_file_set(self):
        solutions = self.vdl_client.query(
                [(  self.file_set_item,
                    vdl.NamedItm(nlp_metadata_source_nr, 
                                 'File set Has Name'),
                    vdl.Unknown('File Set Name'))],
                self.credential_and_key)
        if len(solutions) == 0: raise Exception(
                'File set name not found')
        if len(solutions) > 1: raise Exception(
                'More than one file set name found')
        parsed_file_set = parsed_files.ParsedFileSet(
                solutions[0]['File Set Name'])
        self.add_parse_data(parsed_file_set)
        return parsed_file_set


    def add_parse_data(self, parsed_file_set):
        self.__add_parsed_files__(parsed_file_set)
        for parsed_file_id in parsed_file_set.parsed_files_dict:
            parsed_file_item = vdl.Itm(parsed_file_id)
            self.__add_sections__(parsed_file_set, parsed_file_item)
            self.__add_sentences__(parsed_file_set, parsed_file_item)


    def __add_parsed_files__(self, parsed_file_set):
        parsed_file_x = vdl.Unknown('Parsed File')
        file_x = vdl.Unknown('File')
        solutions = self.vdl_client.query(
            [(  file_x,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'File Is in File set'),
                self.file_set_item),
             (  parsed_file_x,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'Parsed file Is parsed from File'),
                file_x),
             (  file_x,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'Html file Has Url'),
                vdl.Unknown('Url')),
             (  parsed_file_x,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'Parsed file Has Title'),
                vdl.Unknown('Title'))], 
            self.credential_and_key)
        for solution in solutions:
            parsed_file_set.parsed_files_dict[
                    solution['Parsed File'].itemid
                    ] = parsed_files.ParsedFile(
                            solution['Title'], solution['Url'])


    def __add_sections__(
            self, parsed_file_set, parsed_file_item):
        section_x = vdl.Unknown('Section')
        solutions = self.vdl_client.query(
            [(  parsed_file_item,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'Parsed file Has Section'),
                section_x),
             (  section_x,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'Section Has Title'),
                vdl.Unknown('Title'))], 
            self.credential_and_key)
        parsed_file = parsed_file_set.parsed_files_dict[
                parsed_file_item.itemid]
        for solution in solutions:
            section_id = solution['Section'].itemid
            parsed_file_set.sections_dict[section_id
                    ] = parsed_files.Section(solution['Title'])
            parsed_file.section_id_set.add(section_id)


    def __add_sentences__(
            self, parsed_file_set, parsed_file_item):
        section_x = vdl.Unknown('Section')
        sentence_x = vdl.Unknown('Sentence')
        solutions = self.vdl_client.query(
            [(  parsed_file_item,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'Parsed file Has Section'),
                section_x),
             (  section_x,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'Section Has Sentence'),
                sentence_x), 
             (  sentence_x,
                vdl.NamedItm(nlp_metadata_source_nr, 
                             'Sentence Has Text'),
                vdl.Unknown('Text'))], 
            self.credential_and_key)
        parsed_file = parsed_file_set.parsed_files_dict[
                    parsed_file_item.itemid]
        for solution in solutions:
            section_id = solution['Section'].itemid
            sentence_id = solution['Sentence'].itemid
            section = parsed_file_set.sections_dict.get(
                    section_id)
            if section is not None:
                parsed_file_set.sentences_dict[sentence_id
                        ] = parsed_files.Sentence(solution['Text'])
                section.sentence_id_set.add(sentence_id)
