class Section:
    '''
    A section consists of a title (which may be None) plus a
    list of text strings, a list of subsections, and a list of
    sentences obtained by analysing the text strings. It has a
    rank, which determines how it is nested with other sections,
    and a weight, which indicates its importance.
    
    When first created, only the title, rank and weight are set.
    The text strings, subsections and sentences are added later.
    '''
    
    def __init__(self, title, rank, weight):
        '''
        Construct a section
        
        Parameters:
          * title(str): the title of the section.
          * rank(int): the rank of the section, 1 for h1/first level 
            heading, 2 for h2, etc.
          * weight(int): the importance of the section. 
        '''
        
        self.title = title
        self.rank = rank
        self.weight = weight
        self.texts = []
        self.subsections = []
        self.sentences = []
        
    def __repr__(self):
        return 'SECTION(' + str(self.weight) + ') ' + str(self.title) + ' ' + repr(self.texts)
        
    def __str__(self):
        return 'SECTION(' + str(self.weight) + ') ' + str(self.title) + ' ' + str(self.texts)
            
    def add_subsection(self, subsection):
        '''
        Add a subsection.
    
        Parameters:
          * subsection(`Section`): the subsection to be added.
        '''

        self.subsections.append(subsection)
        
    def add_text(self, text):
        '''
        Add a text string.
    
        Parameters:
          * text(str): the text string to be added.
        '''

        self.texts.append(text)

       
    def print(self):
        '''
        Print the section in a format that shows its title, sentences,
        and subsections.
        '''
        print ('SECTION(' + str(self.weight) + ')', self.title, '\n')
        section_text = None
        for sentence in self.sentences:
            if sentence is not None: 
                if section_text is None: section_text = sentence.strip()
                else: section_text += ' ' + sentence.strip()
            if section_text is not None:
                print ('SECTION TEXT', section_text, '\n')
        for subsection in self.subsections:
            subsection.print()
        print('\n')
    
    
class Sentence:
    '''
    A sentence consisting of a text string with an indication of 
    its grammatical complexity
    '''
    
    def __init__(self, text, complexity):
        '''
        Construct a sentence.
        
        Parameters:
          * text(str): the text of the sentence.
          * complexity (int): an indication of the grammatical complexity
            of the sentence.
        '''
        
        self.text = text
        self.complexity = complexity
        
    def __repr__(self):
        return 'SENTENCE(' + str(self.complexity) + ') ' + repr(self.text)
        
    def __str__(self):
        return 'SENTENCE(' + str(self.complexity) + ') '  + str(self.text)
    
