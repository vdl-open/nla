import mammoth


style_map = """
p[style-name='Section Title'] => h1:fresh
p[style-name='Subsection Title'] => h2:fresh
"""


def convert_image(image):
    return ''


with open("ofp_data_integration_white_paper_04.docx", "rb") as docx_file:
    result = mammoth.convert_to_html(docx_file, include_default_style_map=False)
    html = result.value # The generated HTML
    messages = result.messages # Any messages, such as warnings during conversion
    
with open('doc1.htm', 'w') as html_file:
    html_file.write(result.value)
    