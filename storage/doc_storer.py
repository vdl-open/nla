'''
This module stores a document, its sections, and their sentences 
in a virtual data lake. 

'''


from vdlpy import vdl
import changes


nlp_metadata_source_nr = -1008
'''
The numeric identifier of the virtual data lake source containing
metadata for natural language processing.
'''


class Storer():
    def __init__(self, vdl_client, credential_and_key, 
                 data_source_nr, reporter):
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
        self.data_source_nr = data_source_nr
        self.reporter = reporter


    def store_sections(self, section_with_nested_subsections, document_url, 
                           document, read_level):
        '''
        Store a section and its recursively nested subsections and their
        sentences as a document in a virtual data lake, and note that
        the document is loaded.
    
        Parameters:
        * section_with_nested_subsections: a a section with its 
          recursively nested subsections.
        * document_url(str): the document's URL.
        * read_level(vdl.Itm) a vdl Itm object representing the read 
          access level to be set for created items
        
        Returns:
        * The virtual data lake item identifier of the stored document.
        '''
    
        if document is None: 
            document_q = vdl.Unknown('Document')
            change_batches = [[
                vdl.CreateItem(document_q, self.data_source_nr, read_level),
                vdl.PutTriple(document_q, 
                              vdl.NamedItm(nlp_metadata_source_nr, 
                              'Document Has Url'), 
                              document_url)]]
        else: 
            self.__remove_document_sections__(document.itemid, False)
            document_q = document
            change_batches = []
        if section_with_nested_subsections.title is not None:
            if document is None:
                change_batches.append([vdl.PutTriple(
                    document_q, 
                    vdl.NamedItm(nlp_metadata_source_nr, 'Document Has Title'), 
                    section_with_nested_subsections.title)])
            else: change_batches.append([vdl.SetUniqueObject(
                    document,
                    vdl.NamedItm(nlp_metadata_source_nr, 'Document Has Title'), 
                    section_with_nested_subsections.title)])
        self.__add_section_changes__(change_batches, document_q, 
                                section_with_nested_subsections, 
                                'Section0', read_level)
        change_batches.append([vdl.SetUniqueObject(
            document_q,
            vdl.NamedItm(nlp_metadata_source_nr, 'Document Has Load status'),
            'LOADED')])
        document_item = changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, 
                self.reporter).make_changes(
                        change_batches, 'Storing document sections').get('Document')
        if document_item is None: return None
        else: return document_item.itemid


    def remove_document_sections(self, document_id, remove_document):
        '''
        Remove a document and its sections from the virtual data lake.
    
        Parameters:
        * document_id: the vdl item identifier of the document to be removed
        '''
    
        document = vdl.Itm(document_id)
        solutions = self.vdl_client.query(
                [(document, 
                  vdl.NamedItm(nlp_metadata_source_nr, 'Document Has Section'), 
                  vdl.Unknown('Section'))], 
                self.credential_and_key)
        if remove_document: change_batches = [[vdl.DeleteItem(document)]]
        else: change_batches = []
        for solution in solutions:
            change_batches.append([vdl.DeleteItem(solution['Section'])])
        change_batches.append([vdl.RemoveTriples(
                document, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Document Has Load status'))])
        changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, self.reporter).make_changes(
                        change_batches, 'Removing document sections')

           
    def remove_subject_files(self, subject, remove_documents):
        subject_x = vdl.Unknown('Subject')
        document_x = vdl.Unknown('Document')
        unremoved_document_ids = []
        solutions = self.vdl_client.query(
            [(  subject_x, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Subject Has Name'), 
                subject),
             (  document_x, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Document Is on Subject'), 
                subject_x)], 
            self.credential_and_key)
        self.reporter.start_progress_reporting('Removing subject files')
        solution_nr = 0
        for solution in solutions:
            document_id = solution['Document'].itemid
            try :
                self.remove_document_sections(document_id, remove_documents)
            except: 
                unremoved_document_ids.append(document_id)
            solution_nr += 1
            self.reporter.progress('Removing subject files', 
                                   solution_nr/len(solutions))            
            
        if len(unremoved_document_ids) == 0:
            self.reporter.end_progress_reporting('Removing subject files')
        else:
            self.reporter.cancel_progress_reporting('Removing subject files')
            self.reporter.report(
                'Could not remove files with the following identifiers: ' + ' '.join(
                    unremoved_document_ids) + ' Removed ' + str(
                    len(solutions) - len(unremoved_document_ids)) + ' other files.')
   
    
    def __add_section_changes__(self, change_batches, document_q, 
                            section, section_handle, read_level):
        '''
        Add to a list of lists of changes lists of the changes 
        to store a section with its title and sentences
    
        Parameters:
        * change_batches: the list of lists of changes to be added to
        * document_q: the vdl module Unknown or Item representing
          the document containing the section
        * section(`document.Section`): the section to be stored
        * section_handle(str): the handle to be used to represent
          the section in the changes
        * read_level(vdl.Itm) a vdl Itm object representing the read 
          access level to be set for created items
        '''
    
        section_x = vdl.Unknown(section_handle)
        changes = []
        changes.append(vdl.CreateItem(section_x, self.data_source_nr, read_level))
        changes.append(vdl.PutTriple(
            document_q, 
            vdl.NamedItm(nlp_metadata_source_nr, 'Document Has Section'), 
            section_x))
        if section.title is not None:
            changes.append(vdl.PutTriple(
                section_x, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Section Has Title'), 
                section.title))
        i = 0;
        for sentence in section.sentences:
            if sentence.complexity > 10:
                sentence_x = vdl.Unknown(section_handle + '-' + str(i))
                changes.append(vdl.CreateItem(sentence_x, self.data_source_nr))
                changes.append(vdl.PutTriple(
                    section_x, 
                    vdl.NamedItm(nlp_metadata_source_nr, 'Section Has Sentence'), 
                    sentence_x))
                changes.append(vdl.PutTriple(
                    sentence_x, 
                    vdl.NamedItm(nlp_metadata_source_nr, 'Sentence Has Text'), 
                    sentence.text))
                i += 1
        j = 0
        change_batches.append(changes)
        
        for subsection in section.subsections:
            subsection_handle = section_handle + '.' + str(j)
            self.__add_section_changes__(change_batches, document_q, subsection, 
                    section_handle + '.' + str(j), read_level)
            change_batches.append([vdl.PutTriple(
                section_x, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Section Has Subsection'), 
                vdl.Unknown(subsection_handle))])
            j += 1
