'''
This module loads an HTML document from a URL. 
'''

import requests
import traceback
from vdlpy import vdl


nlp_metadata_source_nr = -1008
'''
The numeric identifier of the virtual data lake source containing
metadata for natural language processing.
'''

rank_verb = vdl.NamedItm(-1, 'viewMemberHasDisplayRank')
'''
The virtual data lake view member rank verb
'''
  
  
class Loader:
    
    
    def __init__(
            self, data_source_nr, vdl_url, credential_and_key, 
            public, reporter, test):
        self.data_source_nr = data_source_nr
        self.vdl_client = vdl.Client(vdl_url)
        self.credential_and_key = credential_and_key
        self.public = public
        self.test = test
        self.reporter = reporter
        
        
    def get_subject_version_to_load(self):
        subject_version_x = vdl.Unknown('Subject Version')
        subject_x = vdl.Unknown('Subject')
        if self.test: start_state = 'TEST LOADING'
        else: start_state = 'START LOADING'
        solutions = self.vdl_client.query(
            [(  subject_version_x, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Subject version Is version of Subject'), 
                subject_x),
             (  subject_version_x, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Task Has Progress state'), 
                start_state)], 
            self.credential_and_key)
        if len(solutions) == 0: return None
        subject_version_item_id_set = set(
            [solution['Subject Version'].itemid for solution in solutions])
                
        solutions = self.vdl_client.query(
            [(  subject_version_x, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Subject version Is version of Subject'), 
                subject_x),
             (  subject_version_x, 
                vdl.NamedItm(nlp_metadata_source_nr, 'Task Has Completion state'), 
                vdl.Unknown('State'))], 
            self.credential_and_key)
        loaded_subject_version_item_id_set = {
            solution['Subject Version'].itemid for solution in solutions if len(
                                                    solution['State'].strip()) > 0}
        
        unloaded_subject_version_item_id_set = subject_version_item_id_set.difference(
            loaded_subject_version_item_id_set)
        if len(unloaded_subject_version_item_id_set) == 0: return None
        for unloaded_subject_version_item_id in unloaded_subject_version_item_id_set:
            return vdl.Itm(unloaded_subject_version_item_id)
        
        
    def load_subject_version_files(self, subject_version, reporter):
        self.reporter.start_progress_reporting('Loading files')
        try:
            document_x = vdl.Unknown('Document')
            if self.public: read_level = vdl.public_level()
            else: read_level = None
            solutions = self.vdl_client.query(
                [(  document_x, 
                    vdl.NamedItm(nlp_metadata_source_nr, 'Document Is on Subject version'), 
                    subject_version),
                 (  document_x, 
                    vdl.NamedItm(nlp_metadata_source_nr, 'Document Has Url'), 
                    vdl.Unknown('Url'))], 
                self.credential_and_key)
            
            nr_loaded = 0
            nr_not_loaded = 0
            for solution in solutions:
                url = solution['Url']
                document = solution['Document']
                try:
                    self.__get_sections__(
                        url, document, read_level)
                    nr_loaded += 1
                except Exception: 
                    self.__could_not_load__(url, document, read_level, 
                                            traceback.format_exc())
                    nr_not_loaded += 1
                self.reporter.progress('Loading files', 
                                       (nr_loaded + nr_not_loaded)/len(solutions))
            
            msg = 'Loading complete: ' + str(nr_loaded) + ' files loaded.'
            if nr_not_loaded > 0: msg += ' ' + str(
                                    nr_not_loaded) + ' could not be loaded.'
            self.reporter.report(msg)
            if self.test: self.reporter.note_complete('COMPLETE FOR TEST')
            else: self.reporter.note_complete()
        except Exception:
            self.reporter.report(subject_version, 
                    'Loading subject version failed.' + traceback.format_exc())
        self.reporter.cancel_progress_reporting('Loading files')
    

    def __get_html__(self, url):
        '''
        Get an HTML document.
    
        Parameters:
        * url(str): the document's URL.
    
        Returns:
        * A string containing the HTML content of the document.
        '''

        session = requests.session()
        r = session.get(url, timeout=5)
        if r.status_code != 200:
            raise Exception('Cannot read ', url, 'status', r.status_code)
        return r.text

    def __could_not_load__(self, document_url, document, read_level, message):
        '''
        Note that a document could not be loaded.
    
        Parameters:
        * document_url(str): the document's URL.
        * read_level(vdl.Itm) a vdl Itm object representing the read 
          access level to be set for created items
        * message(str): a message describing the failurew to load
        
        Returns:
        * The virtual data lake item identifier of the document.
        '''
    
        if document is None: 
            document_q = vdl.Unknown('Document')
            changes = [
                vdl.CreateItem(document_q, self.data_source_nr, read_level),
                vdl.PutTriple(document_q, 
                              vdl.NamedItm(nlp_metadata_source_nr, 
                              'Document Has Url'), 
                              document_url)
            ]
        else: 
            document_q = document
            changes = []
        changes.append(vdl.SetUniqueObject(
            document_q,
            vdl.NamedItm(nlp_metadata_source_nr, 'Document Has Load status'),
            'COULD NOT LOAD: ' + message))
        document_item = self.vdl_client.update(
            changes, self.credential_and_key).get('Document')
        if document_item is None: return None
        else: return document_item.itemid
    
    
