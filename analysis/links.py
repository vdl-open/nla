def get_links(concepts):  
    links = []
    for concept in concepts:
        if concept is not None:
            for child_ix in concept.child_ix_set:
                child = concepts[child_ix]
                if child is not None:
                    links.append(Link(concept, child))
    return links


class Link:
    def __init__(self, concept0, concept1):
        self.concept0 = concept0
        self.concept1 = concept1
