import finders
import measures
import clusters as cls
import traceback


NR_OF_DESCRIPTION_TEXTS = 10
MAX_DESCRIPTION_TEXTS_LEN = 1024

        
def get_concepts(finder, sharpness, 
                 min_ref_similarity, reporter):
    concepts = __get_concepts_from_clusters__(
            finder, sharpness, reporter)
    __set_refs__(finder, concepts, concepts[0], 
                 sharpness, min_ref_similarity)
    return concepts
    
    
def __get_concepts_from_clusters__(
        finder, sharpness, reporter):
    if len(finder.concept_clusters) == 0: return None
    activity_name = 'Finding concepts'
    reporter.start_progress_reporting(activity_name)
    concepts = []
    concept_by_cluster_ix = {}
    nr_done = 0
    weighted_cluster_ixs = [(len(
            finder.concept_clusters[ix].own_id_set), ix)
            for ix in range(len(
                finder.concept_clusters))]
    weighted_cluster_ixs.sort(reverse=True)
    first = True
    for wix in weighted_cluster_ixs:
        try:
            cluster_ix = wix[1]
            if __add_or_merge_concept__(
                    finder, first, cluster_ix, concepts, 
                    concept_by_cluster_ix, sharpness):
                first = False
            reporter.progress(
                    activity_name, 
                    nr_done/len(weighted_cluster_ixs))
            nr_done += 1
        except:
            traceback.print_exc()
            raise
    reporter.cancel_progress_reporting(activity_name)
    return concepts


def __add_or_merge_concept__(
        finder, first, cluster_ix, concepts, 
        concept_by_cluster_ix, sharpness):                        
    parent_concept = __parent_concept__(
            cluster_ix, finder.concept_clusters,
            concept_by_cluster_ix)
    if not first and parent_concept is None:
        return False
    cluster = finder.concept_clusters[cluster_ix]
    similarities = finder.get_similarities()
    centroid = similarities.centroid(
            [finder.index.vectors_dict[own_id] 
             for own_id in cluster.own_id_set])
    distinct_relevant_ids = finder.distinct_relevant_ids(
            centroid, cluster.own_id_set, sharpness, 
            NR_OF_DESCRIPTION_TEXTS, 
            MAX_DESCRIPTION_TEXTS_LEN)
    if first or parent_concept is None: level = 0
    else: level = parent_concept.level + 1
    concept = __concept__(
            finder, centroid, cluster.own_id_set, 
            level, sharpness, distinct_relevant_ids)
    (similarly_described_concept
     ) = __similarly_described_concept__(
            concept, concepts, similarities, sharpness)
    if similarly_described_concept is None: 
        add_concept(cluster_ix, concept, parent_concept, 
                concepts, concept_by_cluster_ix)
    else: __merge_cluster_into_concept__(
            cluster_ix, cluster.own_id_set, 
            similarly_described_concept, 
            concept_by_cluster_ix)
    return True


def __parent_concept__(
        cluster_ix, concept_clusters, 
        concept_by_cluster_ix): 
    parent_cluster_ix = cls.parent_ix(
            cluster_ix, concept_clusters)
    if parent_cluster_ix is None: return None
    parent_concept = concept_by_cluster_ix.get(
            parent_cluster_ix)
    if parent_concept is None: return None
    if len(parent_concept.child_ixs) >= 6: 
            return None
    if parent_concept.level > 1: return None      
    else: return parent_concept

     
def __similarly_described_concept__(
        new_concept, concepts, similarities, sharpness):
    for concept in concepts:
        sim = similarities.similarity(
                concept.description_vector, 
                new_concept.description_vector)
        if sim >= sharpness: return concept
    return None


def add_concept(
        cluster_ix, concept, parent_concept, 
        concepts, concept_by_cluster_ix):
    print('ADD CON', concept.description)
    concept.concept_ix = len(concepts) 
    if parent_concept is None: parent_ix = None
    else: parent_ix = parent_concept.concept_ix                
    concepts.append(concept)
    concept_by_cluster_ix[cluster_ix] = concept
    if parent_concept is not None: 
        parent_concept.child_ixs.append(
                concept.concept_ix)
    
    
def __merge_cluster_into_concept__(
        cluster_ix, cluster_own_id_set, 
        similarly_described_concept, 
        concept_by_cluster_ix):
    print('MERGE CLUSTER INTO', 
          similarly_described_concept.description)
    concept_by_cluster_ix[cluster_ix
            ] = similarly_described_concept
    similarly_described_concept.text_id_set.update(
            cluster_own_id_set)
    similarly_described_concept.vector = None
    
    
def __concept__(finder, central_vector, cluster_id_set,
                 level, sharpness, distinct_relevant_ids):
    encoder = finder.get_encoder()
    similarities = finder.get_similarities()
    distinct_relevant_texts = [
        finder.index.texts_dict[text_id]
        for text_id in distinct_relevant_ids]
    long_title = finder.summarizer.get_long_title(
            distinct_relevant_texts, central_vector, 
            encoder, similarities)
    (title, description
     ) = __get_title_and_description__(long_title)                    
    description_vector = encoder.encode(
            description)
    return Concept(central_vector, title, description, 
            description_vector, cluster_id_set, level,
            distinct_relevant_ids)


def __get_title_and_description__(long_title):
    words = long_title.split()
    if len(words) <= 6: 
        return (long_title, long_title)
    else:
        title = ' '.join(words[:5]) + ' ...'
        return(title, long_title)
    
    
# Recursively set the references of a concept 
# and its descendants
def __set_refs__(
        finder, concepts, concept, sharpness,
        min_ref_similarity):
    # Exclude references that are in descendant concepts
    text_id_set = concept.text_id_set.copy()
    for ix in concept.child_ixs:
        child_concept = concepts[ix]
        # The text ids of the child include the text ids 
        # of its descendants
        text_id_set.difference_update(
                child_concept.text_id_set)            
        __set_refs__(finder, concepts, child_concept,
                     sharpness, min_ref_similarity)
        
    # Set the concept vector if it is None. (This will be
    # the case if the concept has had clusters added to it.)
    similarities = finder.get_similarities()
    if concept.vector is None:
        concept.vector = similarities.centroid(
            [finder.index.vectors_dict[text_id]
            for text_id in concept.text_id_set])
            
    # Remove references that are distant from the
    # concept vector
    relevant_text_id_set = {
            text_id for text_id in text_id_set
                if similarities.similarity(
                    finder.index.vectors_dict[
                            text_id], 
                    concept.vector
                ) >= min_ref_similarity}.union(
                    concept.distinct_relevant_ids)
        
    # Remove near-duplicates that are in the same section
    concept.reference_text_id_set = (
            relevant_text_id_set.difference(
                finder.__near_duplicate_id_set__(
                        relevant_text_id_set, sharpness)))
    
    print('CONCEPT', concept.title)
    finder.get_summary(
        concept.vector, concept.reference_text_id_set)
    
    finder.get_refs_html(
        concept.vector, concept.reference_text_id_set)


class Concept:
    def __init__(self, vector, title, description, 
                 description_vector, text_id_set, level,
                 distinct_relevant_ids):
        self.concept_ix = None
        self.title = title
        self.vector = vector
        self.description = description
        self.description_vector = description_vector
        self.text_id_set = text_id_set.copy()
        self.level = level
        self.distinct_relevant_ids = distinct_relevant_ids
        self.child_ixs = []
        self.reference_text_id_set = None
        self.references_html = None


    def weight(self):
        return len(self.text_id_set)
