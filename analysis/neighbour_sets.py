def get_separated_sets_dict(
        target_number, focus, vectors_dict, encoder, reporter):
    (ids, overlapping_neighbour_sets
     ) = __get_ids_and_sets__(focus, vectors_dict, encoder, reporter)
    return __get_separated_sets_dict__(
        ids, overlapping_neighbour_sets, target_number, reporter)
    
    
def __get_ids_and_sets__(focus, vectors_dict, encoder, reporter):
    if vectors_dict is None or len(vectors_dict) == 0:
        return None
    activity_name = 'Finding neighbour sets'
    reporter.start_progress_reporting(activity_name)
    ids = list(vectors_dict.keys())
    neighbour_sets = [{identity} for identity in ids]
    nr_of_steps = int(len(ids)*(len(ids)-1)/2)
    step = 0
    for i in range(len(ids)):
        id_i = ids[i]
        neighbour_set = neighbour_sets[i]
        vector = vectors_dict[id_i]
        for j in range(i+1, len(ids)):
            id_j = ids[j]
            similarity = encoder.similarity(
                    vector, vectors_dict[id_j])
            if similarity >= focus: 
                neighbour_set.add(id_j)
                neighbour_sets[j].add(id_i)
            step += 1
            reporter.progress(activity_name, step/nr_of_steps)
    reporter.cancel_progress_reporting(activity_name)
    return (ids, neighbour_sets)
                 

def __get_separated_sets_dict__(
        ids, neighbour_sets, target_number, reporter):
    if ids is None or len(ids) == 0: return None
    if neighbour_sets is None or len(neighbour_sets) == 0: 
        return None
    if len(neighbour_sets) != len(ids): return None
    activity_name = 'Finding separated neighbour sets'
    reporter.report('Finding separated neighbour sets')
    (sorted_ids, sorted_neighbour_sets
     ) = __sorted_by_neighbourhood_size__(ids, neighbour_sets)
    separated_ixs = []
    for i in range(len(sorted_ids)):
        principal_id = sorted_ids[i]
        overlap = False
        for separated_ix in separated_ixs:
            if principal_id in sorted_neighbour_sets[separated_ix]:
                overlap = True
                break
        if not overlap:
            separated_ixs.append(i)
            if len(separated_ixs) >= target_number: break
    return {sorted_ids[ix]: sorted_neighbour_sets[ix] 
             for ix in separated_ixs}


def __sorted_by_neighbourhood_size__(ids, neighbour_sets):
    trips = [(len(neighbour_sets[i]), ids[i], neighbour_sets[i]) 
             for i in range(len(ids))]
    trips.sort(reverse=True)
    return ([trip[1] for trip in trips], [trip[2] for trip in trips])
