FOCUS_STEP_SIZE = 0.05


class Grid:
    def __init__(self):
        self.similarities_dict = {}
        
        
    def add(self, id0, id1, similarity):
        if id0 < id1: 
            self.similarities_dict[(id0, id1)] = similarity
        else: self.similarities_dict[(id1, id0)] = similarity
        
        
    def get_similarity(self, id0, id1):
        if id0 < id1:
            return self.similarities_dict.get((id0, id1))
        elif id0 > id1: 
            return self.similarities_dict.get((id1, id0))
        else: return 1.0
        
        
    def id_with_most_neighbours(self, similarity):
        if len(self.similarities_dict) == 0: return None
        return __sorted_counts_and_ids__(similarity)[0][1]  
        
        
    def counts_and_separated_ids_with_most_neighbours(
            self, nr_to_get, similarity):
        if len(self.similarities_dict) == 0: return None
        counts_and_ids = self.__sorted_counts_and_ids__(similarity)  
        counts_and_separated_ids = [counts_and_ids[0]]
        n = 1
        i = 1
        while n < nr_to_get and i < len(counts_and_ids):
            separated = True
            id = counts_and_ids[i][1]
            for m in range(n):
                if self.get_similarity(
                    id, counts_and_separated_ids[m][1]) >= similarity:
                    separated = False
                    break
            if separated:
                counts_and_separated_ids.append(counts_and_ids[i])
                n += 1
            i += 1
        return counts_and_separated_ids
    
    
    def neighbour_set(self, principal_id, similarity):
        neighbour_set = {principal_id}
        for ((id0, id1), sim) in self.similarities_dict.items():
            if id0 == principal_id and sim >= similarity:
                neighbour_set.add(id1)
            elif id1 == principal_id and sim >= similarity:
                neighbour_set.add(id0)
        return neighbour_set
    
    
    def most_central_id(self, exclusion_set=None):
        if len(self.similarities_dict) == 0: return None
        sorted_weights_and_ids = self.sorted_weights_and_ids(
                exclusion_set) 
        if len(sorted_weights_and_ids) == 0: return None
        return sorted_weights_and_ids[0][1]
   
    
    def most_central_weights_and_separated_ids(
            self, nr_to_get, similarity):
        if len(self.similarities_dict) == 0: return None
        weights_and_ids = self.sorted_weights_and_ids()  
        weights_and_separated_ids = [weights_and_ids[0]]
        n = 1
        i = 1
        while n < nr_to_get and i < len(weights_and_ids):
            separated = True
            w_id = weights_and_ids[i][1]
            for m in range(n):
                if self.get_similarity(
                    w_id, weights_and_separated_ids[m][1]) >= similarity:
                    separated = False
                    break
            if separated:
                weights_and_separated_ids.append(weights_and_ids[i])
                n += 1
            i += 1
        return weights_and_separated_ids
   
    
    def separated_weighted_ids(
            self, sorted_weighted_ids, nr_to_get, similarity):
        if len(sorted_weighted_ids) == 0: return None
        weights_and_separated_ids = [sorted_weighted_ids[0]]
        n = 1
        i = 1
        while n < nr_to_get and i < len(sorted_weighted_ids):
            separated = True
            w_id = sorted_weighted_ids[i][1]
            for m in range(n):
                if self.get_similarity(
                    w_id, weights_and_separated_ids[m][1]) >= similarity:
                    separated = False
                    break
            if separated:
                weights_and_separated_ids.append(sorted_weighted_ids[i])
                n += 1
            i += 1
        return weights_and_separated_ids
    
    
    def concentration(self, central_id):
        weights = []
        for ((id0, id1), sim) in self.similarities_dict.items():
            if id0 == central_id or id1 == central_id: 
                weights.append(sim)
        weights.sort(reverse=True)
        half_weight = sum(weights)/2
        cumulative_weight = 0.0
        for i in range(len(weights)):
           cumulative_weight += weights[i]
           if cumulative_weight >= half_weight:
               return weights[i]
        return 0.0
    
    
    def heaviest_step(self, central_id, step_size):
        nr_of_steps = int(1/step_size)
        if nr_of_steps < 3: return None
        steps = [0]*nr_of_steps
        for ((id0, id1), sim) in self.similarities_dict.items():
            if id0 == central_id or id1 == central_id:
                if sim < 0: step = 0
                else: step = int(sim/FOCUS_STEP_SIZE)
                if step >= nr_of_steps: step = nr_of_steps-1
                steps[step] += sim
        indexed_steps = [(steps[i], i) 
                          for i in range(nr_of_steps)]   
        step = max(indexed_steps)[1]     
        return (step*step_size, (step+1)*step_size)     
    
    
    def focused_set(self, central_id):
        nr_of_steps = int(1/FOCUS_STEP_SIZE)
        if nr_of_steps < 3: return None
        steps = [0]*nr_of_steps
        id_sets = [set() for s in range(nr_of_steps)]
        for ((id0, id1), sim) in self.similarities_dict.items():
            if id0 == central_id or id1 == central_id:
                if sim < 0: step = 0
                else: step = int(sim/FOCUS_STEP_SIZE)
                if step >= nr_of_steps: step = nr_of_steps-1
                steps[step] += sim
                if id0 == central_id: id_sets[step].add(id1)
                else: id_sets[step].add(id0)
        indexed_steps = [(steps[i], i) 
                          for i in range(nr_of_steps)]        
        heaviest_ix = max(indexed_steps)[1]
        cutoff_weight = steps[heaviest_ix]/2
        for i in range(heaviest_ix-1, 0, -1):
            if steps[i] < cutoff_weight: 
                focus = i*FOCUS_STEP_SIZE
                id_set = set()
                for j in range(i, nr_of_steps):
                    id_set.update(id_sets[j])
                weight = sum([steps[j] 
                              for j in range(i, nr_of_steps)])
                return FocusedSet(central_id, focus, id_set, weight)
        return None       
    
    
    def __sorted_counts_and_ids__(self, similarity):
        neighbour_count_dict = {}
        for ((id0, id1), sim) in self.similarities_dict.items():
            if sim >= similarity:
                count0 = neighbour_count_dict.get(id0)
                if count0 is None: 
                    neighbour_count_dict[id0] = 1
                else: neighbour_count_dict[id0] = count0 + 1
                count1 = neighbour_count_dict.get(id1)
                if count1 is None: 
                    neighbour_count_dict[id1] = 1
                else: neighbour_count_dict[id1] = count1 + 1
        counts_and_ids = [(count, id) for (id, count
                            ) in neighbour_count_dict.items()]
        counts_and_ids.sort(reverse = True)
        return counts_and_ids
    
    
    def sorted_weights_and_ids(self, exclusion_set=None):
        weight_dict = {}
        for ((id0, id1), sim) in self.similarities_dict.items():
            if (exclusion_set is None) or ((
                    id0 not in exclusion_set) and (
                    id1 not in exclusion_set)):
                weight0 = weight_dict.get(id0)
                if weight0 is None: 
                    weight_dict[id0] = sim
                else: weight_dict[id0] = weight0 + sim
                weight1 = weight_dict.get(id1)
                if weight1 is None: 
                    weight_dict[id1] = sim
                else: weight_dict[id1] = weight1 + sim
        weights_and_ids = [(weight, id) for (id, weight
                            ) in weight_dict.items()]
        weights_and_ids.sort(reverse = True)
        return weights_and_ids
    
    
class FocusedSet:
    def __init__(self, central_id, focus, id_set, weight):
        self.central_id = central_id
        self.focus = focus
        self.id_set = id_set
        self.weight = weight
        