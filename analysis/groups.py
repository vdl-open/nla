class Groups:
    def __init__(self):
        self.groups_dict = {}
        
        
    def add_member(self, member):
        if member not in self.groups_dict:
            self.groups_dict[member] = {member}
        
        
    def merge_groups(self, member0, member1):
        group0 = self.groups_dict[member0]
        if member1 in group0: return
        group1 = self.groups_dict[member1]
        group0.update(group1)
        for member in group1:
            self.groups_dict[member] = group0
            
            
    def members_in_same_group(self, member0, member1):
        return member1 in self.groups_dict[member0]
        