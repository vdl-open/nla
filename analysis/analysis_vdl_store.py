import requests
from vdlpy import vdl
from utils import changes
from utils import reports


NLP_METADATA_SOURCE_NR = -1008
'''
The numeric identifier of the virtual data lake source containing
metadata for natural language processing.
'''


class Store():
    def __init__(self, vdl_client, credential_and_key):
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
    
    
    # Get the set of item ids of file sets that have
    # analyses
    def get_analysed_id_set(self):
        analysis_x = vdl.Unknown('Analysis')
        parsed_file_set_x = vdl.Unknown('Parsed File Set')
        file_set_x = vdl.Unknown('File Set')
        
        solutions = self.vdl_client.query(
                [(  analysis_x, 
                    vdl.NamedItm(
                            NLP_METADATA_SOURCE_NR, 
                            'Analysis Is of File set'), 
                    file_set_x)], 
                self.credential_and_key)
        return {solution['File Set'].itemid 
                for solution in solutions}
            
        
    def new_analysis_item(
            self, file_set_item, source_nr, read_level):
        analysis_handle = 'Analysis'
        start_changes = []
        start_changes.append(vdl.CreateItem(
                analysis_handle, 
                source_nr, 
                read_level))
        start_changes.append(vdl.PutTriple(
                vdl.Unknown(analysis_handle),
                vdl.NamedItm(
                        NLP_METADATA_SOURCE_NR, 
                        'Analysis Is of File set'),
                file_set_item))
        return self.vdl_client.update(
                start_changes, self.credential_and_key
                        )[analysis_handle]
                
    
    def store_concepts(self, analysis_item, concepts, 
                       source_nr, access_level, reporter):   
        change_batches = []
        self.__add_store_concepts_changes__(
                change_batches, analysis_item, concepts,
                source_nr, access_level)
        activity_name = 'Storing concepts'
        stored_items_dict = changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, reporter
                    ).make_changes(change_batches, 'Storing concepts')
        return {concepts[int(handle)].concept_ix: stored_item.itemid 
                for (handle, stored_item) in stored_items_dict.items()}
        
    
    def store_links(
            self, analysis_item, stored_concept_id_by_ix_dict, 
            links, source_nr, access_level, reporter):   
        change_batches = []
        self.__add_store_links_changes__(
            change_batches, stored_concept_id_by_ix_dict, 
            analysis_item, links, source_nr, access_level)
        activity_name = 'Storing links'
        changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, reporter
                    ).make_changes(change_batches, 'Storing links')


    def get_incomplete_analysis_id_set(self):
        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Analysis'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Analysis Is of File set'),
                vdl.Unknown('File Set'))],
            self.credential_and_key)
        analysis_item_id_set = {solution['Analysis'].itemid
                                for solution in solutions}

        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Analysis'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Analysis Is Complete'),
                True)],
            self.credential_and_key)
        complete_analysis_item_id_set = {solution['Analysis'].itemid
                                for solution in solutions}
        
        return analysis_item_id_set.difference(
                complete_analysis_item_id_set)


    def note_analysed(self, analysis_item):
        self.vdl_client.update(
            [vdl.SetUniqueObject(
                analysis_item,
                vdl.NamedItm(NLP_METADATA_SOURCE_NR,
                             'Analysis Is Complete'),
                True)],
            self.credential_and_key)
    
    
    def delete_analysis(self, analysis_item):
        self.vdl_client.update(        
                self.__get_delete_analysis_changes__(analysis_item),
                self.credential_and_key)
        
        
    def delete_orphans(self):
        # Delete the orphaned links first because they
        # won't be found when the concepts that they link
        # have been deleted
        self.vdl_client.update(        
                self.__get_delete_orphaned_link_changes__(),
                self.credential_and_key)
        self.vdl_client.update(        
                self.__get_delete_orphaned_concept_changes__(),
                self.credential_and_key)
        

    def __add_store_concepts_changes__(
            self, change_batches, analysis_item, concepts, 
            source_nr, read_level):

        i = 0
        for concept in concepts:
            if concept is not None:
                handle = str(i)
                changes = []
                changes.append(vdl.CreateItem(
                        handle, 
                        source_nr, 
                        read_level))
                changes.append(vdl.PutTriple(
                        vdl.Unknown(handle),
                        vdl.NamedItm(
                                NLP_METADATA_SOURCE_NR, 
                                'Concept Is in Analysis'),
                        analysis_item))
                changes.append(vdl.PutTriple(
                        vdl.Unknown(handle),
                        vdl.NamedItm(
                                NLP_METADATA_SOURCE_NR, 
                                'Concept Has Title'),
                        concept.title))
                changes.append(vdl.PutTriple(
                        vdl.Unknown(handle),
                        vdl.NamedItm(
                                NLP_METADATA_SOURCE_NR, 
                                'Concept Has Summary'),
                        concept.summary))
                changes.append(vdl.PutTriple(
                        vdl.Unknown(handle),
                        vdl.NamedItm(
                                NLP_METADATA_SOURCE_NR, 
                                'Concept Has References html'),
                        concept.references_html))
                changes.append(vdl.PutTriple(
                        vdl.Unknown(handle),
                        vdl.NamedItm(
                                NLP_METADATA_SOURCE_NR, 
                                'Concept Has Weight'),
                        float(concept.weight())))
                change_batches.append(changes)
            i += 1


    def __add_store_links_changes__(
            self, change_batches, stored_concept_id_by_ix_dict, 
            analysis_item, links, source_nr, read_level):
        if links is None: return

        i = 0
        for link in links:
            handle = str(i)
            changes = []
            changes.append(vdl.CreateItem(
                    handle, 
                    source_nr, 
                    read_level))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Link Is in Analysis'),
                    analysis_item))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Link Links Concept'),
                    vdl.Itm(stored_concept_id_by_ix_dict[
                        link.concept0.concept_ix])))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Link Links Concept'),
                    vdl.Itm(stored_concept_id_by_ix_dict[
                        link.concept1.concept_ix])))
            change_batches.append(changes)
            i += 1
        

    def __get_delete_analysis_changes__(self, analysis_item):
        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Concept'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Concept Is in Analysis'),
                analysis_item)],
            self.credential_and_key)
        deletion_changes = [vdl.DeleteItem(solution['Concept'])
                             for solution in solutions]

        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Link'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Link Is in Analysis'),
                analysis_item)],
            self.credential_and_key)
        deletion_changes.extend([vdl.DeleteItem(solution['Link'])
                             for solution in solutions])
        
        deletion_changes.append(vdl.DeleteItem(analysis_item))        
        return deletion_changes


    def __get_delete_orphaned_concept_changes__(self):
        concept_solutions = self.vdl_client.query(
            [(  vdl.Unknown('Concept'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Concept Is in Analysis'),
                vdl.Unknown('Analysis'))],
            self.credential_and_key)
        concept_ids = {concept_solution['Concept'].itemid
                    for concept_solution in concept_solutions}
        titled_concept_solutions = self.vdl_client.query(
            [(  vdl.Unknown('Concept'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Concept Has Title'),
                vdl.Unknown('Title'))],
            self.credential_and_key)
        titled_concept_ids = {
                titled_concept_solution['Concept'].itemid
                for titled_concept_solution in titled_concept_solutions}
        orphaned_concept_ids = titled_concept_ids.difference(
                concept_ids)
        return [vdl.DeleteItem(vdl.Itm(orphaned_concept_id))
                for orphaned_concept_id in orphaned_concept_ids]


    def __get_delete_orphaned_link_changes__(self):
        link_solutions = self.vdl_client.query(
            [(  vdl.Unknown('Link'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Link Is in Analysis'),
                vdl.Unknown('Analysis'))],
            self.credential_and_key)
        link_ids = {link_solution['Link'].itemid
                    for link_solution in link_solutions}
        concept_link_solutions = self.vdl_client.query(
            [(  vdl.Unknown('Link'),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Link Links Concept'),
                vdl.Unknown('Concept'))],
            self.credential_and_key)
        concept_link_ids = {
                concept_link_solution['Link'].itemid
                for concept_link_solution in concept_link_solutions}
        orphaned_link_ids = concept_link_ids.difference(link_ids)
        return [vdl.DeleteItem(vdl.Itm(orphaned_link_id))
                for orphaned_link_id in orphaned_link_ids]
