import clusters as cls
import concept_finder
import links as lnks
from indexing import indexes


class Finder:
    def __init__(self, encoder, similarities, summarizer, 
                 parse_store, index_store, analysis_store):
        self.encoder = encoder
        self.similarities = similarities
        self.summarizer = summarizer
        self.parse_store = parse_store
        self.index_store = index_store
        self.analysis_store = analysis_store
        self.__init_data__()
        
        
    def __init_data__(self):
        self.ids = None
        self.parsed_file_by_section_dict = None
        self.section_by_sentence_dict = None
        self.clusters = None
        self.index = None
        self.concepts = None
        self.links = None

    
    def get_encoder(self):
        return self.encoder
    
    
    def get_similarities(self):
        return self.similarities
    
       
    def find(self, file_set_item, analysis_item, sharpness, 
             base_focus, source_nr, access_level, reporter):   
        self.__init_data__()
        reporter.report('Loading index')
        self.index = self.index_store.load_index(file_set_item)
        if self.index is None: raise Exception('No index')
        self.ids = list(self.index.vectors_dict)
        if len(self.ids) == 0: raise Exception('Empty index')
        reporter.report('Building parsed file by section dictionary')
        self.parsed_file_by_section_dict = (
                indexes.get_parsed_file_by_section_dict(self.index))
        reporter.report('Building section by sentence dictionary')
        self.section_by_sentence_dict = (
                indexes.get_section_by_sentence_dict(self.index))
        reporter.report('Dictionaries built - clustering')
        self.clusters = cls.Clusters(
                self.ids, self.index.vectors_dict, 
                self.similarities, reporter)
        reporter.report(
                'Clustered ' + str(len(self.clusters.ids)
                ) + ' items in ' + (
                self.clusters.clustering_time()))
        self.concepts = concept_finder.get_concepts(
                self, sharpness, base_focus, reporter)
        reporter.report(
                'Storing ' + str(
                    len(self.concepts)) + ' concepts')
        (stored_concept_id_by_ix_dict
         ) = self.analysis_store.store_concepts(
                analysis_item, self.concepts, source_nr, 
                access_level, reporter)
        self.links = lnks.get_links(self.concepts)
        reporter.report(
                'Storing ' + str(
                    len(self.links)) + ' links')
        self.analysis_store.store_links(
                analysis_item, stored_concept_id_by_ix_dict, 
                self.links, source_nr, access_level, reporter)
        if len(self.links) == 0:
            reporter.report(
                'Found ' + str(len(self.concepts)
                           ) + ' concepts and no links')
        else:
            reporter.report(
                'Found ' + str(len(self.concepts)
                            ) + ' concepts and ' + (
                               str(len(self.links))
                            ) + ' links')
            
    
    def concept_clusters(self):
        return self.clusters.concept_clusters
    
      
    def distinct_relevant_ids(
            self, central_vector, text_ids, 
            sharpness, max_nr):
        if len(text_ids) == 0: return None
        texts_dict = self.index.texts_dict
        vectors_dict = self.index.vectors_dict
        weighted_text_ids = [
                (self.similarities.similarity(
                    central_vector, vectors_dict[text_id]), 
                 text_id) for text_id in text_ids]
        weighted_text_ids.sort(reverse=True)
        first_id = weighted_text_ids[0][1]
        distinct_relevant_ids = [first_id]
        nr = max(len(weighted_text_ids), max_nr)
        for weighted_text_id in weighted_text_ids[:nr]:
            text_id = weighted_text_id[1]
            separated = True
            for distinct_relevant_id in distinct_relevant_ids:
                if self.similarities.similarity(
                        vectors_dict[distinct_relevant_id], 
                        vectors_dict[text_id]) >= sharpness:
                    separated = False
                    break
            if separated:
                distinct_relevant_ids.append(text_id)
                if len(distinct_relevant_ids) >= max_nr:
                    break           
        return distinct_relevant_ids
            
        
    def __near_duplicate_id_set__(self, text_id_set, sharpness):
        near_duplicate_id_set = set()
        distinct_child_ids_dict = {}
        for text_id in text_id_set:
            parent_id = self.section_by_sentence_dict.get(text_id)
            if parent_id is None:
                parent_id = self.parsed_file_by_section_dict.get(text_id)
            if parent_id is not None and self.__id_has_duplicate_text__(
                    text_id, parent_id, distinct_child_ids_dict,
                    sharpness):
                near_duplicate_id_set.add(text_id)    
        return near_duplicate_id_set
    
    
    def __id_has_duplicate_text__(
            self, text_id, parent_id, 
            distinct_child_ids_dict, sharpness):
        text = self.index.texts_dict[text_id]
        distinct_sibling_id_set = distinct_child_ids_dict.get(
                parent_id)
        if distinct_sibling_id_set is None:
             distinct_child_ids_dict[parent_id] = {text_id}
             return False
        else:
            vectors_dict = self.index.vectors_dict
            near_duplicate = False
            vector = vectors_dict[text_id]
            for sibling_id in distinct_sibling_id_set:
                if self.similarities.similarity(
                        vector, vectors_dict[sibling_id]
                        ) >= sharpness:
                    near_duplicate = True
                    break
            return near_duplicate
                
    
    def get_refs_html(self, ref_ids):
        lines = ['<p>See the following.</p>', '<ul>']
        for ref_id in ref_ids:
            sentence = self.index.sentences_dict.get(ref_id)
            if sentence is None:
                section_id = ref_id
                section = self.index.sections_dict[
                        section_id]
            else: 
                sentence_id = ref_id
                section_id = self.section_by_sentence_dict[
                        sentence_id]
                section = self.index.sections_dict[section_id]
            parsed_file_id = self.parsed_file_by_section_dict[
                    section_id]
            parsed_file = self.index.parsed_files_dict[
                    parsed_file_id]
            if sentence is None: line = '<li>'
            else: line = '<li><b>' + sentence.text + '</b> in '
            line += 'Section <i>' + section.title + (
                    '</i> of <a href="' + parsed_file.file_url
                    ) + ('" target="_blank">'
                    ) + parsed_file.title + '</a></li>'
            lines.append(line)
        lines.append('</ul>')
        return '\n'.join(lines)
                
    
    def get_summary_and_refs_html(self, vector, text_ids):
        weighted_ids = [(
            self.similarities.similarity(
                vector, self.index.vectors_dict[text_id]), 
            text_id)
                for text_id in text_ids]
        weighted_ids.sort(reverse=True)
        texts = [self.index.texts_dict[weighted_id[1]]
                 for weighted_id in weighted_ids]
        summary = self.summarizer.get_summary(texts)
        lines = ['<p>See the following.</p>', '<ul>']
        for weighted_id in weighted_ids:
            sentence = self.index.sentences_dict.get(
                    weighted_id[1])
            if sentence is None:
                section_id = weighted_id[1]
                section = self.index.sections_dict[
                        section_id]
            else: 
                sentence_id = weighted_id[1]
                section_id = self.section_by_sentence_dict[
                        sentence_id]
                section = self.index.sections_dict[section_id]
            parsed_file_id = self.parsed_file_by_section_dict[
                    section_id]
            parsed_file = self.index.parsed_files_dict[
                    parsed_file_id]
            if sentence is None: line = '<li>'
            else: line = '<li><i>' + sentence.text + '</i> in '
            line += 'Section <i>' + section.title + (
                    '</i> of <a href="' + parsed_file.file_url
                    ) + ('">' + parsed_file.title + '</a></li>')
            lines.append(line)
        lines.append('</ul>')
        return(summary, '\n'.join(lines))

    
    def get_refs_html0(self, vector, ref_id_set):
        (first_ref_by_parsed_file_set_dict, 
         first_ref_by_section_dict
         ) = self.__get_ref_dicts__(vector, ref_id_set)
        weighted_parsed_file_ids = [
            (i, parsed_file_id) for (parsed_file_id, i
             ) in first_ref_by_parsed_file_set_dict.items()]
        weighted_parsed_file_ids.sort(reverse=True)
        lines = ['<p>See the following documents and sections.</p>']
        for weighted_parsed_file_id in weighted_parsed_file_ids:
            self.__add_parsed_file_lines__(
                lines, weighted_parsed_file_id[1], 
                first_ref_by_section_dict)
        return '\n'.join(lines)


    def __get_ref_dicts__(self, vector, ref_id_set):
        weighted_ref_ids = [(
            self.similarities.similarity(
                vector,
                self.index.vectors_dict[ref_id]),
            ref_id) 
        for ref_id in ref_id_set]
        weighted_ref_ids.sort(reverse=True)
        ref_ids = [weighted_ref_id[1]
                   for weighted_ref_id in weighted_ref_ids]
        first_ref_by_parsed_file_set_dict = {}
        first_ref_by_section_dict = {}
        for i in range(len(ref_ids)):
            ref_id = ref_ids[i]
            if ref_id in self.index.sections_dict:
                sentence_id = None
                section_id = ref_id
            else:
                sentence_id = ref_id
                section_id = (
                    self.section_by_sentence_dict.get(
                            sentence_id))
            if section_id is not None:
                if first_ref_by_section_dict.get(section_id
                                                 ) is None:
                    first_ref_by_section_dict[section_id] = i
                    parsed_file_id = (
                        self.parsed_file_by_section_dict.get(
                            section_id))
                    if first_ref_by_parsed_file_set_dict.get(
                            parsed_file_id) is None:
                        first_ref_by_parsed_file_set_dict[
                            parsed_file_id] = i
        return(first_ref_by_parsed_file_set_dict, 
               first_ref_by_section_dict)


    def __add_parsed_file_lines__(
            self, lines, parsed_file_id, 
            first_ref_by_section_dict):
        parsed_file = self.index.parsed_files_dict[
                parsed_file_id]
        lines.append(
            '<h4><a href="' + parsed_file.file_url + '">' + (
                parsed_file.title + '</a></h4>'))
        weighted_section_ids = []
        for section_id in parsed_file.section_id_set:
            section_title = self.index.sections_dict[
                    section_id].title
            
            if section_title is not None and section_title != (
                    parsed_file.title):
                ref = first_ref_by_section_dict.get(section_id)
                if ref is not None:
                    weighted_section_ids.append((ref, section_id))
        if len(weighted_section_ids) > 0:
            lines.append('<ul>')
            weighted_section_ids.sort(reverse=True)
            for weighted_section_id in weighted_section_ids:
                section_title = self.index.sections_dict[
                    weighted_section_id[1]].title
                lines.append('<li>' + section_title + '</li>')
                lines.append('</ul>')
  
    
    
    
   