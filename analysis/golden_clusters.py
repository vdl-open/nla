
'''
Perform heirarchical clustering and identify golden 
clusters. 

Heirarchical clustering proceeds by recursively 
merging the pair of closest clusters until only a single 
cluster remains. In the course of doing this, clusters of 
sufficient weight are identified as nuggets. The nuggets
are then refined by removing members that were not in 
clusters of sufficient weight, and the resulting golden 
clusters are returned.

The identifiers to be clustered are given as a list. A
list of base clusters of the identifiers is given also;
the clustering starts from these. 

The weight of a cluster is the total number of identifiers
in the base clusters from which it was formed.

The clustering is performed on the indexes of base clusters
in the list of base clusters, rather than on the identifiers 
in the list of identifiers.
'''
def golden_clusters(
        ids, distances, vectors_dict, base_clusters,
        reporter):
    nuggets = __nuggets__(
        ids, distances, vectors_dict, base_clusters, 
        reporter)
    return __refined_clusters__(nuggets, reporter)


def weight(cluster, base_clusters):
    if base_clusters is None: return len(cluster)
    else: return sum([len(base_clusters[i])
                      for i in cluster])
    

def __nuggets__(
        ids, distances, vectors_dict, base_clusters,
        reporter):
    activity_name = 'Finding clusters'
    reporter.start_progress_reporting(activity_name)
    (clusters, cluster_distances, cluster_weights
     ) = __clusters_distances_and_weights__(
            ids, distances, base_clusters)
    min_nugget_weight = int(
            sum([cluster_weights[i] 
                 for i in range(len(clusters))])/100)
    print('MIN NS', min_nugget_weight)
    nuggets = []
    nr_to_do = len(clusters) - 1
    while len(clusters) > 1:
        closest_pair = __closest_pair__(
                clusters, cluster_distances, 
                cluster_weights)
        # If the lighter cluster is at least the minimum
        # weight, include both. If it is not, then it is
        # too light, and the heavier cluster is not 
        # significantly different from its parent, so
        # don't include either cluster        
        lighter_cluster_ix = closest_pair[1]
        if cluster_weights[lighter_cluster_ix
                           ] >= min_nugget_weight:
            heavier_cluster = clusters[closest_pair[0]]
            nuggets.append(heavier_cluster.copy())
            nuggets.append(clusters[
                    lighter_cluster_ix].copy())
        __merge_pair__(
                closest_pair, clusters, 
                cluster_distances, cluster_weights)
        reporter.progress(
                activity_name, 
                (nr_to_do - len(clusters))/nr_to_do)
    reporter.cancel_progress_reporting(activity_name)
    nuggets.append(clusters[0])
    return nuggets
   
    
def __refined_clusters__(nuggets, reporter): 
    reporter.report('Refining clusters')
    refined_clusters = []   
    for ix in range(len(nuggets)):
        ch_ixs = child_ixs(ix, nuggets)
        if len(ch_ixs) == 0:
            cluster = nuggets[ix].copy()
        else:
            cluster = []
            for ch_ix in ch_ixs:
                cluster.extend(nuggets[ch_ix])
        refined_clusters.append(cluster)
    return refined_clusters
        
    
def parent_ix(ix, golden_clusters): 
    elt = golden_clusters[ix][0]
    for i in range(ix+1, len(golden_clusters)):
        if elt in golden_clusters[i]: return i
    return None
        
    
def child_ixs(ix, golden_clusters):
    parent_cluster = golden_clusters[ix]
    child_ixs = [] 
    for i in range(ix-1, -1, -1):
        elt_i = golden_clusters[i][0]
        if elt_i in parent_cluster: 
            child = True
            for j in child_ixs:
                if elt_i in golden_clusters[j]:
                    child = False
                    break
            if child: child_ixs.append(i)
    return child_ixs


def __clusters_distances_and_weights__(
        ids, distances, base_clusters):
    l = len(base_clusters) 
    clusters = [[i] for i in range(l)]
    cluster_distances = [[None]*l for _ in range(l)]
    for i in range(l):
        for j in range(i+1, l):
            cluster_distances[i][j
                    ] = __cluster_distance__(
                        base_clusters[i], 
                        base_clusters[j], 
                        distances)
    cluster_weights = [len(base_clusters[i]) 
                        for i in range(l)]
    return (clusters, cluster_distances, cluster_weights)


def __cluster_distance__(
        base_cluster_i, base_cluster_j, distances):
    distance = 0.0
    for i in base_cluster_i:
        for j in base_cluster_j:
            try:
                if i<j: distance += distances[i][j]
                else: distance += distances[j][i]
            except:
                print('!!!', i, j)
                raise
    return distance


def __closest_pair__(
        clusters, cluster_distances, cluster_weights):
    l = len(clusters)
    closest_pair = None
    least_avg_distance = None
    for i in range(l):
        weight_i = cluster_weights[i]
        for j in range(i+1, l):
            dist = cluster_distances[i][j]
            weight = weight_i*cluster_weights[j]
            avg_dist = dist/weight
            if (closest_pair is None) or (
                    avg_dist < least_avg_distance):
                closest_pair = (i, j)
                least_avg_distance = avg_dist
    if cluster_weights[closest_pair[1]
            ] > cluster_weights[closest_pair[0]]:
        closest_pair = (closest_pair[1], closest_pair[0])
    print('CD', least_avg_distance)
    return closest_pair
                                    
                                    
def __merge_pair__(
        pair, clusters, 
        cluster_distances, cluster_weights):
    extended_cluster_ix = pair[0]
    discarded_cluster_ix = pair[1]
    for i in range(len(cluster_distances)):
        if i < discarded_cluster_ix:
            distance_to_transfer = cluster_distances[
                    i][discarded_cluster_ix]
        elif i > discarded_cluster_ix:
            distance_to_transfer = cluster_distances[
                    discarded_cluster_ix][i]
        else: distance_to_transfer = 0
        if i < extended_cluster_ix:
                cluster_distances[i][extended_cluster_ix
                        ] += distance_to_transfer
        elif i > extended_cluster_ix:
                cluster_distances[extended_cluster_ix][i
                        ] += distance_to_transfer
    del cluster_distances[discarded_cluster_ix]
    for ds in cluster_distances:
        del ds[discarded_cluster_ix]
    clusters[extended_cluster_ix].extend(
            clusters[discarded_cluster_ix])
    cluster_weights[extended_cluster_ix
            ] += cluster_weights[discarded_cluster_ix]
    del cluster_weights[discarded_cluster_ix]
    del clusters[discarded_cluster_ix]
      
                                         