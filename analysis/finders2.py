import texts_and_vectors as tvs
import neighbour_sets
import containing_set_pairs


focus = 0.25
sharpness = 0.75
nlp_metadata_source_nr = -1008


class Finder:
    def __init__(self, parsed_subject_version, encoder, 
                 summarizer, analysis_store, reporter):
        self.parsed_subject_version = parsed_subject_version
        self.texts_and_vectors = tvs.TextsAndVectors(
                parsed_subject_version, encoder)
        self.encoder = encoder
        self.summarizer = summarizer
        self.reporter = reporter
        self.analysis_store = analysis_store
        self.section_by_sentence_dict = self.__get_section_by_sentence_dict__()
        self.document_by_section_dict = self.__get_document_by_section_dict__()
      
      
    def find_concepts_and_links(
                self, analysis_item, target_number):
        separated_sets_dict = neighbour_sets.get_separated_sets_dict(
                target_number, focus, self.texts_and_vectors.vectors_dict, 
                self.encoder, self.reporter)                
        concepts = self.__get_concepts__(separated_sets_dict)
        if concepts is None:
            self.reporter.report('No concepts found')  
            return      
        concepts.sort(key=__weight__, reverse=True)
        initial_min_cutoff = min(8, len(concepts))
        initial_max_cutoff = min(32, len(concepts))
        first_cut_relevant_concepts = concepts[:initial_max_cutoff]
        self.__set_vector_and_similarities__(first_cut_relevant_concepts)
        (own_text_ids_dict, concept_ix_pair_text_ids_dict
         ) = self.__get_text_id_dicts__(first_cut_relevant_concepts)
        (relevant_concepts, relevant_links
         ) = self.__get_relevant_concepts_and_links__(
            first_cut_relevant_concepts, 
            own_text_ids_dict, concept_ix_pair_text_ids_dict, 
            initial_min_cutoff, initial_max_cutoff)
        self.__add_concept_refs__(relevant_concepts, own_text_ids_dict)
        stored_concept_id_by_principal_id_dict = self.analysis_store.store_concepts(
                analysis_item, relevant_concepts, self.reporter)
        self.analysis_store.store_links(
                analysis_item, stored_concept_id_by_principal_id_dict, 
                relevant_links, self.reporter)
        self.reporter.report(
            'Found ' + str(len(stored_concept_id_by_principal_id_dict)
                           ) + ' concepts ' + str(len(relevant_links)
                                                  ) + ' links')
 
    
    def __get_document_by_section_dict__(self): 
        document_by_section_dict = {}    
        for (document_id, document
             ) in self.parsed_subject_version.documents_dict.items():
            for section_id in document.section_id_set:
                text = self.texts_and_vectors.texts_dict[section_id]
                document_by_section_dict[section_id] = document_id
        return document_by_section_dict
 
    
    def __get_section_by_sentence_dict__(self): 
        section_by_sentence_dict = {}    
        for (section_id, section
             ) in self.parsed_subject_version.sections_dict.items():
            for sentence_id in section.sentence_id_set:
                text = self.texts_and_vectors.texts_dict[sentence_id]
                section_by_sentence_dict[sentence_id] = section_id
        return section_by_sentence_dict
 
    
    def __get_concepts__(self, separated_sets_dict):         
        if separated_sets_dict is None or len(separated_sets_dict) == 0: 
            return None
        activity_name = 'Forming concepts'
        self.reporter.start_progress_reporting(activity_name)
        concepts = []
        previous_long_title_vectors = []
        separated_set_nr = 0
        nr_of_separated_sets = len(separated_sets_dict)
        for (principal_id, id_set) in separated_sets_dict.items():
            concept = self.__concept__(principal_id, id_set)
            long_title_vector = self.encoder.encode(concept.description)
            distinct = True
            for i in range(len(previous_long_title_vectors)):
                previous_long_title_vector = previous_long_title_vectors[i]
                sim = self.encoder.similarity(
                        previous_long_title_vector, long_title_vector)
                if sim >= focus:
                    concepts[i] = self.__merged_concept__(concepts[i], concept)
                    distinct = False
                    break
            if distinct:
                previous_long_title_vectors.append(long_title_vector)
                concepts.append(concept)
            separated_set_nr += 1
            self.reporter.progress(activity_name, 
                                   separated_set_nr/nr_of_separated_sets)
        self.reporter.cancel_progress_reporting(activity_name)
        return concepts   
    
    
    def __get_relevant_concepts_and_links__(
            self, concepts, own_text_ids_dict, 
            concept_ix_pair_text_ids_dict, 
            initial_min_cutoff, initial_max_cutoff):
        min_cutoff = initial_min_cutoff
        max_cutoff = initial_max_cutoff
        while min_cutoff < max_cutoff - 1:
            cutoff = int((min_cutoff + max_cutoff)/2)
            relevant_concepts = concepts[:cutoff]
            relevant_links = self.__get_relevant_links_for_concepts__(
                    relevant_concepts, concept_ix_pair_text_ids_dict)
            if self.__max_links_per_concept__(relevant_links) > 6:
                max_cutoff = cutoff
            else: min_cutoff = cutoff
        cutoff = min_cutoff
        relevant_concepts = concepts[:cutoff]
        relevant_links = self.__get_relevant_links_for_concepts__(
                relevant_concepts, concept_ix_pair_text_ids_dict)
        return(relevant_concepts, relevant_links)
        
        
    def __get_text_id_dicts__(self, concepts):
        activity_name = 'Finding text dictionaries'
        own_text_ids_dict = {}
        concept_ix_pair_text_ids_dict = {}
        searcher = containing_set_pairs.Searcher(
                [concept.text_id_set for concept in concepts])
        for text_id in self.texts_and_vectors.texts_dict:
            ix_or_pair = searcher.search(text_id)
            if ix_or_pair is None: pass
            elif isinstance(ix_or_pair, int):
                key = concepts[ix_or_pair].principal_id
                id_set = own_text_ids_dict.get(key)
                if id_set is None: own_text_ids_dict[key] = {text_id}
                else: id_set.add(text_id)
            else:
                id_set = concept_ix_pair_text_ids_dict.get(ix_or_pair)
                if id_set is None: 
                    concept_ix_pair_text_ids_dict[ix_or_pair] = {text_id}
                else: id_set.add(text_id)
        return(own_text_ids_dict, concept_ix_pair_text_ids_dict)


    def __add_concept_refs__(self, concepts, own_text_ids_dict):
        for concept in concepts:
            own_text_id_set = own_text_ids_dict.get(
                                    concept.principal_id)
            concept.reference_text_weights_dict = {}
            if own_text_id_set is not None: 
                # Remove near-duplicates that are in the same section
                deduped_text_id_set = own_text_id_set.difference(
                    self.__near_duplicate_id_set__(own_text_id_set))
                reference_sims_and_text_ids = [(sim, text_id) 
                    for (text_id, sim
                         ) in concept.text_similarities_dict.items()
                            if text_id in deduped_text_id_set]
                reference_sims_and_text_ids.sort(reverse=True)
                i = 0
                for (sim, text_id) in reference_sims_and_text_ids:
                    if i < 8 and (i > 0 or sim >= focus):
                        concept.reference_text_weights_dict[
                                text_id] = sim
                        i += 1
                    else: break
    

    def __get_relevant_links_for_concepts__(
            self, concepts, concept_ix_pair_text_ids_dict):
        if concepts is None or len(concepts) == 0: return None
        if concept_ix_pair_text_ids_dict is None or len(
                concept_ix_pair_text_ids_dict) == 0: 
            return None
        links = []
        nr_of_concepts = len(concepts)
        for (pair, pair_text_id_set
             ) in concept_ix_pair_text_ids_dict.items(): 
            if (pair[0] < nr_of_concepts and pair[1] < nr_of_concepts
                    ) and len(pair_text_id_set) > 0:
                link = self.__link__(
                    concepts[pair[0]], concepts[pair[1]], pair_text_id_set)
                if link is not None: links.append(link) 
        relevant_links =  __get_most_relevant_links__(concepts, links)
        connected_concepts_dict = {concept.principal_id: [concept.principal_id]
                                   for concept in concepts}
        for link in relevant_links: 
            __update_connected_concepts_dict__(connected_concepts_dict, link)
        if len(connected_concepts_dict.values()) == 1: return relevant_links
        other_links = [link for link in links if link not in relevant_links]
        other_links.sort(key=__weight__, reverse=True)
        for link in other_links:
            if __link_connects_unconnected_concepts__(
                    link, connected_concepts_dict):
                __update_connected_concepts_dict__(connected_concepts_dict, link)
                relevant_links.append(link)
                if len(connected_concepts_dict.values()) == 1: break
        return relevant_links
                
    
    def __max_links_per_concept__(self, links):
        nr_of_links_per_concept_dict = {}
        for link in links:
            self.__inc_nr_of_lincs__(
                link.concept0_principal_id, nr_of_links_per_concept_dict)
            self.__inc_nr_of_lincs__(
                link.concept1_principal_id, nr_of_links_per_concept_dict)
        return max(nr_of_links_per_concept_dict.values())
    
    
    def __inc_nr_of_lincs__(self, principal_id, 
                            nr_of_links_per_concept_dict):
        n = nr_of_links_per_concept_dict.get(principal_id)
        if n is None: nr_of_links_per_concept_dict[principal_id] = 1
        else: nr_of_links_per_concept_dict[principal_id] = n+1
        
    
    def __concept__(self, principal_id, neighbour_set):
        principal_vector = self.texts_and_vectors.vectors_dict[principal_id]
        weighted_text_ids= [(
                self.encoder.similarity(principal_vector, 
                        self.texts_and_vectors.vectors_dict[neighbour_id]), 
                neighbour_id) for neighbour_id in neighbour_set]
        weighted_text_ids.sort(reverse=True)
        texts = self.__distinct_relevant_texts__(
                principal_id, neighbour_set, 3000)
        long_title = self.summarizer.get_long_title(
                texts, principal_vector, self.encoder)
        if long_title is None: 
            long_title = self.texts_and_vectors.texts_dict[principal_id] 
        (title, description) = __get_title_and_description__(long_title)
        return Concept(title, description, principal_id,  
                       neighbour_set, len(neighbour_set))
                
                      
    def __merged_concept__(self, concept0, concept1):
        text_id_set = concept0.text_id_set.union(concept1.text_id_set)
        return Concept(
            concept0.title, concept0.description, 
            concept0.principal_id, text_id_set, len(text_id_set))
        
    def __distinct_relevant_texts__(self, principal_id, ids, max_length):
        encoder = self.encoder
        principal_vector = self.texts_and_vectors.vectors_dict[principal_id]
        weighted_non_principal_ids = [(encoder.similarity(
                self.texts_and_vectors.vectors_dict[np_id], 
                principal_vector), np_id)
                        for np_id in ids if np_id != principal_id]
        weighted_non_principal_ids.sort(reverse=True)
        distinct_relevant_ids = [principal_id]
        principal_text = self.texts_and_vectors.texts_dict[principal_id]
        distinct_relevant_texts = [principal_text]
        distinct_relevant_texts_length = len(principal_text)
        distinct_relevant_vectors = [
                self.texts_and_vectors.vectors_dict[principal_id]]
        for weighted_non_principal_id in weighted_non_principal_ids:
            np_id = weighted_non_principal_id[1]
            vector = self.texts_and_vectors.vectors_dict[np_id]
            separated = True
            for i in range(len(distinct_relevant_ids)):
                if encoder.similarity(
                        vector, distinct_relevant_vectors[i]) >= sharpness:
                    separated = False
                    break
            if separated:
                np_text = self.texts_and_vectors.texts_dict[np_id]
                if distinct_relevant_texts_length + len(np_text) > max_length:
                    break
                distinct_relevant_texts_length += len(np_text)
                distinct_relevant_ids.append(np_id)
                distinct_relevant_texts.append(np_text)
                distinct_relevant_vectors.append(vector)
        return distinct_relevant_texts
    
    
    def __link__(self, concept0, concept1, pair_text_id_set):
        deduped_text_id_set = pair_text_id_set.difference(
            self.__near_duplicate_id_set__(pair_text_id_set))
        sims_and_text_ids = []
        for text_id in deduped_text_id_set:
            sim0 = concept0.text_similarities_dict.get(text_id)
            sim1 = concept1.text_similarities_dict.get(text_id)
            if sim0 is not None and sim1 is not None:
                sims_and_text_ids.append((min(sim0, sim1), text_id))
        sims_and_text_ids.sort(reverse=True)
        reference_text_weights_dict = {}
        i = 0
        for (sim, text_id) in sims_and_text_ids:
            if i < 8 and sim >= focus:
                reference_text_weights_dict[text_id] = sim
                i += 1
            else: break
        if len(reference_text_weights_dict) == 0: return None
        else: return Link(concept0.principal_id, concept1.principal_id, 
                    reference_text_weights_dict)


    def __near_duplicate_id_set__(self, text_id_set):
        near_duplicate_id_set = set()
        distinct_child_ids_dict = {}
        for text_id in text_id_set:
            parent_id = self.section_by_sentence_dict.get(text_id)
            if parent_id is None:
                parent_id = self.document_by_section_dict.get(text_id)
            if parent_id is not None and self.__id_has_duplicate_text__(
                    text_id, parent_id, distinct_child_ids_dict):
                near_duplicate_id_set.add(text_id)    
        return near_duplicate_id_set
    
    
    def __id_has_duplicate_text__(
            self, text_id, parent_id, distinct_child_ids_dict):
        text = self.texts_and_vectors.texts_dict[text_id]
        distinct_sibling_id_set = distinct_child_ids_dict.get(parent_id)
        if distinct_sibling_id_set is None:
             distinct_child_ids_dict[parent_id] = {text_id}
             return False
        else:
            vectors_dict = self.texts_and_vectors.vectors_dict
            near_duplicate = False
            vector = vectors_dict[text_id]
            for sibling_id in distinct_sibling_id_set:
                if self.encoder.similarity(
                        vector, vectors_dict[sibling_id]
                        ) >= sharpness:
                    near_duplicate = True
                    break
            return near_duplicate


    def __set_vector_and_similarities__(self, concepts):
        for concept in concepts:
            concept.vector = self.encoder.encode(
                                concept.description)
            concept.text_similarities_dict = {
                    text_id: self.encoder.similarity(
                        concept.vector, 
                        self.texts_and_vectors.vectors_dict[text_id])
                    for text_id in concept.text_id_set}


class Concept:
    def __init__(self, title, description, principal_id,  
                 text_id_set, weight):
        self.title = title
        self.description = description
        self.principal_id = principal_id
        self.text_id_set = text_id_set
        self.weight = weight
        self.vector = None
        self.text_similarities_dict = None
        self.reference_text_weights_dict = None
        
        
    def reference_text_ids(self):
        return list(self.reference_text_weights_dict)          
        
    
class Link:
    def __init__(self, concept0_principal_id, concept1_principal_id, 
                 reference_text_weights_dict):
        self.concept0_principal_id = concept0_principal_id
        self.concept1_principal_id = concept1_principal_id
        self.reference_text_weights_dict = reference_text_weights_dict
        self.weight = sum(reference_text_weights_dict.values())
        
        
    def reference_text_ids(self):
        return list(self.reference_text_weights_dict)          
        
        
def __get_most_relevant_links__(concepts, links): 
    concept_link_ix_set_dict = {}
    for link_ix in range(len(links)):
        link = links[link_ix]
        concept_link_ix_set = concept_link_ix_set_dict.get(
                link.concept0_principal_id)
        if concept_link_ix_set is None: 
            concept_link_ix_set_dict[link.concept0_principal_id] = {link_ix}
        else: concept_link_ix_set.add(link_ix)
        concept_link_ix_set = concept_link_ix_set_dict.get(
                link.concept1_principal_id)
        if concept_link_ix_set is None: 
            concept_link_ix_set_dict[link.concept1_principal_id] = {link_ix}
        else: concept_link_ix_set.add(link_ix)
        
    relevant_link_ix_set = set()
    for concept in concepts:
        link_ix_set = concept_link_ix_set_dict.get(concept.principal_id) 
        if link_ix_set is not None:
            max_link_weight = max([links[ix].weight for ix in link_ix_set])
            cutoff = 3*max_link_weight/4
            relevant_link_ix_set.update(
                    [ix for ix in link_ix_set 
                        if links[ix].weight >= cutoff])
    return [links[ix] for ix in relevant_link_ix_set]


def __update_connected_concepts_dict__(connected_concepts_dict, link):
    connected_concepts0 = connected_concepts_dict[
            link.concept0_principal_id]
    connected_concepts1 = connected_concepts_dict[
            link.concept1_principal_id]
    if connected_concepts0 == connected_concepts1: return
    connected_concepts = connected_concepts0 + connected_concepts1
    for principal_id in connected_concepts:
        connected_concepts_dict[principal_id] = connected_concepts


def __link_connects_unconnected_concepts__(
        link, connected_concepts_dict):
    connected_concepts0 = connected_concepts_dict[
            link.concept0_principal_id]
    connected_concepts1 = connected_concepts_dict[
            link.concept1_principal_id]
    return connected_concepts0 != connected_concepts1


def __get_title_and_description__(long_title):
    words = long_title.split()
    if len(words) <= 6: 
        return (long_title, long_title)
    else:
        title = ' '.join(words[:5]) + ' ...'
        return(title, long_title)


def __weight__(weighted_object):
    return weighted_object.weight  

