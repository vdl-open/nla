import texts_and_vectors as tvs
import measures
import radius_clusters as rcls
import golden_clusters as gcls
import concepts as cons
import links as lnks
import ideas as ides
import statistics
import numpy as np


MAXTEXTLEN = 3000


class Finder:
    def __init__(self, parsed_subject_version, encoder, 
                 summarizer, analysis_store, reporter):
        self.parsed_subject_version = parsed_subject_version
        self.texts_and_vectors = tvs.TextsAndVectors(
                parsed_subject_version, encoder, reporter)
        self.encoder = encoder
        self.summarizer = summarizer
        self.analysis_store = analysis_store
        reporter.report('Building dictionaries')
        self.document_by_section_dict = (
                self.__get_document_by_section_dict__())
        self.section_by_sentence_dict = (
                self.__get_section_by_sentence_dict__())
        self.ids = list(self.texts_and_vectors.vectors_dict)
        self.radius_clusters = []
        self.golden_clusters = []
        self.ideas = []
        self.concepts = []
        self.links = []

             
    def find(self, analysis_item, target_number, 
            sharpness, min_ref_similarity, reporter):
        similarities = measures.similarities(
                self.ids, self.texts_and_vectors.vectors_dict, 
                self.encoder, reporter)
        distances = measures.distances_from_similarities(
                similarities)
        self.radius_clusters = [[i] 
                for i in range(len(self.ids))]
        #self.radius_clusters = rcls.radius_clusters(
        #        self.ids, similarities, sharpness)
        #ideas = ides.get_ideas(radius_clusters, texts_dict)
        self.golden_clusters = gcls.golden_clusters(
                self.ids, distances, 
                self.texts_and_vectors.vectors_dict, 
                self.radius_clusters, reporter)
        self.concepts = cons.get_concepts(
                self, sharpness, min_ref_similarity, 
                reporter)
        (stored_concept_id_by_ix_dict
         ) = self.analysis_store.store_concepts(
                analysis_item, self.concepts, reporter)
        self.links = lnks.get_links(self.concepts)
        self.analysis_store.store_links(
                analysis_item, stored_concept_id_by_ix_dict, 
                self.links, reporter)
        if len(self.links) == 0:
            reporter.report(
                'Found ' + str(len(self.concepts)
                           ) + ' concepts and no links')
        else:
            reporter.report(
                'Found ' + str(len(self.concepts)
                            ) + ' concepts and ' + (
                               str(len(self.links))
                            ) + ' links')
    
    
    def golden_cluster_ids(self, golden_cluster):
        golden_cluster_ids = []
        for i in golden_cluster:
            for j in self.radius_clusters[i]:
                golden_cluster_ids.append(self.ids[j])
        return golden_cluster_ids
        
        
    def principal_ids(self, golden_cluster):
        return [self.ids[self.radius_clusters[i][0]]
                for i in golden_cluster]

        
    def __get_document_by_section_dict__(self): 
        document_by_section_dict = {}    
        for (document_id, document
             ) in self.parsed_subject_version.documents_dict.items():
            for section_id in document.section_id_set:
                text = self.texts_and_vectors.texts_dict[section_id]
                document_by_section_dict[section_id] = document_id
        return document_by_section_dict
 
    
    def __get_section_by_sentence_dict__(self): 
        section_by_sentence_dict = {}    
        for (section_id, section
             ) in self.parsed_subject_version.sections_dict.items():
            for sentence_id in section.sentence_id_set:
                text = self.texts_and_vectors.texts_dict[sentence_id]
                section_by_sentence_dict[sentence_id] = section_id
        return section_by_sentence_dict
            
        
    def __distinct_relevant_texts__(
            self, central_vector, text_ids, sharpness):
        texts_dict = self.texts_and_vectors.texts_dict
        vectors_dict = self.texts_and_vectors.vectors_dict
        weighted_text_ids = [
                (self.encoder.similarity(
                    central_vector, 
                    self.texts_and_vectors.vectors_dict[text_id]), 
                 text_id) for text_id in text_ids]
        weighted_text_ids.sort(reverse=True)
        first_id = weighted_text_ids[0][1]
        distinct_relevant_ids = [first_id]
        first_text = texts_dict[first_id]
        distinct_relevant_texts = [first_text]
        distinct_relevant_texts_length = len(first_text)
        for weighted_text_id in weighted_text_ids:
            text_id = weighted_text_id[1]
            separated = True
            for distinct_relevant_id in distinct_relevant_ids:
                if self.encoder.similarity(
                        vectors_dict[distinct_relevant_id], 
                        vectors_dict[text_id]) >= sharpness:
                    separated = False
                    break
            if separated:
                text = texts_dict[text_id]
                if distinct_relevant_texts_length + len(text
                        ) > MAXTEXTLEN:
                    break
                distinct_relevant_texts_length += len(text)
                distinct_relevant_ids.append(text_id)
                distinct_relevant_texts.append(text)
        return distinct_relevant_texts
            
        
    def __near_duplicate_id_set__(self, text_id_set, sharpness):
        near_duplicate_id_set = set()
        distinct_child_ids_dict = {}
        for text_id in text_id_set:
            parent_id = self.section_by_sentence_dict.get(text_id)
            if parent_id is None:
                parent_id = self.document_by_section_dict.get(text_id)
            if parent_id is not None and self.__id_has_duplicate_text__(
                    text_id, parent_id, distinct_child_ids_dict,
                    sharpness):
                near_duplicate_id_set.add(text_id)    
        return near_duplicate_id_set
    
    
    def __id_has_duplicate_text__(
            self, text_id, parent_id, distinct_child_ids_dict,
            sharpness):
        text = self.texts_and_vectors.texts_dict[text_id]
        distinct_sibling_id_set = distinct_child_ids_dict.get(
                parent_id)
        if distinct_sibling_id_set is None:
             distinct_child_ids_dict[parent_id] = {text_id}
             return False
        else:
            vectors_dict = self.texts_and_vectors.vectors_dict
            near_duplicate = False
            vector = vectors_dict[text_id]
            for sibling_id in distinct_sibling_id_set:
                if self.encoder.similarity(
                        vector, vectors_dict[sibling_id]
                        ) >= sharpness:
                    near_duplicate = True
                    break
            return near_duplicate
       