import finders
import clusters as cls
import traceback


MAX_RELEVANT_IDS = 25
MAXCHILDREN = 12
        
def get_concepts(
        finder, sharpness, base_focus, reporter):
    concepts = __get_concepts__(
            finder, sharpness, sharpness, reporter)
    if concepts is None or len(
            concepts) == 0: return None
    __set_refs__(
            finder, concepts, concepts[0], sharpness)
    return concepts

    
def __get_concepts__(finder, sharpness, focus, reporter):   
    activity_name = 'Finding concepts'
    reporter.start_progress_reporting(activity_name)
    concepts = []
    concept_by_cluster_ix = {}
    nr_done = 0
    concept_clusters = finder.concept_clusters()
    if concept_clusters is None or len(
            concept_clusters) == 0: return None
    weighted_cluster_ixs = [(len(
            concept_clusters[ix]), ix)
            for ix in range(len(concept_clusters))]
    weighted_cluster_ixs.sort(reverse=True)
    first = True
    nr_of_concepts = 0
    for wix in weighted_cluster_ixs:
        try:
            cluster_ix = wix[1]
            if __add_or_merge_concept__(
                    finder, first, cluster_ix, concepts, 
                    concept_by_cluster_ix, sharpness, focus,
                    reporter):
                if len(concepts) > nr_of_concepts:
                    nr_of_concepts = len(concepts)
                first = False
            reporter.progress(
                    activity_name, 
                    nr_done/len(weighted_cluster_ixs))
            nr_done += 1
        except: raise
    reporter.cancel_progress_reporting(activity_name)
    return concepts


def __add_or_merge_concept__(
        finder, first, cluster_ix, concepts, 
        concept_by_cluster_ix, sharpness, focus,
        reporter):
    concept_clusters = finder.concept_clusters()                  
    parent_concept = __parent_concept__(
            cluster_ix, concept_clusters,
            concept_by_cluster_ix)
    if not first and parent_concept is None:
        return False
    
    similar_concept = None
    cluster = concept_clusters[cluster_ix]
    similarities = finder.get_similarities()
    central_vector = similarities.centroid([
            finder.index.vectors_dict[text_id] 
            for text_id in cluster])
    if parent_concept is not None and (
            similarities.similarity(
                    parent_concept.vector,
                    central_vector) > focus): 
        similar_concept = parent_concept
    else:
        distinct_relevant_ids = finder.distinct_relevant_ids(
                central_vector, cluster, sharpness,
                MAX_RELEVANT_IDS)
        if distinct_relevant_ids is None: 
            similar_concept = parent_concept
        else:
            concept = None
            if parent_concept is not None and (
                    parent_concept.distinct_relevant_ids_match(
                        distinct_relevant_ids, 
                        finder.index.vectors_dict,
                        similarities, focus)):
                similar_concept = parent_concept
            else:
                similar_concept = __similarly_vectored_concept__(
                        central_vector, concepts, similarities,
                        focus)
                
    if similar_concept is None:
        if first or parent_concept is None: level = 0
        else: level = parent_concept.level + 1
        concept = __concept__(
                finder, central_vector, cluster, 
                level, distinct_relevant_ids, reporter)
        if concept is None: 
            if parent_concept is None:
                raise Exception('Cannot form main concept')
            else: similar_concept = parent_concept 
        else: 
            similar_concept = __similarly_titled_concept__(
                    concept, concepts, similarities, 
                    focus)
            
    if similar_concept is None: 
        __add_concept__(cluster_ix, concept, parent_concept, 
                    concepts, concept_by_cluster_ix, 
                    similarities)
    else: 
        __merge_cluster_into_concept__(
            finder, cluster_ix, cluster, 
            similar_concept, concept_by_cluster_ix)
    return True


def __parent_concept__(
        cluster_ix, concept_clusters, 
        concept_by_cluster_ix): 
    parent_cluster_ix = cls.parent_ix(
            cluster_ix, concept_clusters)
    if parent_cluster_ix is None: return None
    parent_concept = concept_by_cluster_ix.get(
            parent_cluster_ix)
    if parent_concept is None: return None
    #if len(parent_concept.child_ix_set) >= MAXCHILDREN: 
    if __too_many_children__(
            cluster_ix, concept_clusters, 
            parent_concept): return None
    if parent_concept.level > 1: return None      
    else: return parent_concept


def __too_many_children__(
        cluster_ix, clusters, parent_concept):
    if parent_concept.nominal_size is None:
        return False
    min_viable_child_size = parent_concept.nominal_size - (
            MAXCHILDREN-len(parent_concept.child_ix_set)
            )*parent_concept.nominal_diff
    return len(clusters[cluster_ix]
               ) < min_viable_child_size

    
def __similarly_vectored_concept__(
        vector, concepts, similarities, focus):
    for concept in concepts:
        if similarities.similarity(
                concept.vector, vector
            ) >= focus: 
            return concept
    return None

     
def __similarly_titled_concept__(
        new_concept, concepts, similarities, focus):
    for concept in concepts:
        if similarities.similarity(
                concept.title_vector, 
                new_concept.title_vector
            ) >= focus: 
            return concept
    return None


def __add_concept__(
        cluster_ix, concept, parent_concept, 
        concepts, concept_by_cluster_ix, similarities):
    concept.concept_ix = len(concepts) 
    if parent_concept is None: parent_ix = None
    else: parent_ix = parent_concept.concept_ix                
    concepts.append(concept)
    concept_by_cluster_ix[cluster_ix] = concept
    if parent_concept is not None: 
        parent_concept.child_ix_set.add(
                concept.concept_ix)
        if len(parent_concept.child_ix_set) == 4:
            parent_concept.nominal_size = sum(
                [len(concepts[ix].text_id_set)
                 for ix in parent_concept.child_ix_set])/4
            parent_concept.nominal_diff = max(
                    parent_concept.nominal_size - len(
                        concept.text_id_set), 1)
    
    
def __merge_cluster_into_concept__(
        finder, cluster_ix, cluster, 
        similar_concept, concept_by_cluster_ix):
    concept_by_cluster_ix[cluster_ix
            ] = similar_concept
    l = len(similar_concept.text_id_set)
    similar_concept.text_id_set.update(
            cluster)
    if len(similar_concept.text_id_set) != l:
        (similar_concept.vector
         ) = finder.similarities.centroid(
                [finder.index.vectors_dict[vector_id]
                 for vector_id in (
                     similar_concept.text_id_set)])
    
    
def __concept__(finder, central_vector, cluster, 
                level, distinct_relevant_ids, reporter):
    encoder = finder.get_encoder()
    similarities = finder.get_similarities()
    distinct_relevant_texts = [
        finder.index.texts_dict[text_id]
        for text_id in distinct_relevant_ids]
    (summary, title, l
     ) = finder.summarizer.get_summary_and_title(
            distinct_relevant_texts, reporter)
    if title is None: 
        return None
    title_vector = encoder.encode(title)
    return Concept(central_vector, title, title_vector, 
            summary, cluster, level, 
            distinct_relevant_ids[:l])

    
# Recursively set the references of a concept 
# and its descendants
def __set_refs__(
        finder, concepts, concept, sharpness):
    # Exclude references that are in descendant concepts
    text_id_set = concept.text_id_set.copy()
    for ix in concept.child_ix_set:
        child_concept = concepts[ix]
        if child_concept is not None:
            # The text ids of the child include the text ids 
            # of its descendants. Don't include them in the
            # references (unless they were used to generate the 
            # summary and title - see below)
            text_id_set.difference_update(
                    child_concept.text_id_set)            
            __set_refs__(
                    finder, concepts, child_concept, sharpness)
        
    # Ensure that all texts that were used to generate the 
    # summary and title are included in the references
    text_id_set.update(concept.title_ids)
            
    # Set the concept vector if it is None. (This should 
    # not ever be the case.)
    similarities = finder.get_similarities()
    if concept.vector is None:
        concept.vector = similarities.centroid(
            [finder.index.vectors_dict[text_id]
            for text_id in concept.text_id_set])
        
    # Sort the ids by their vectors' distances from
    # the concept vector
    weighted_ids = [
        (similarities.similarity(
            concept.vector, 
            finder.index.vectors_dict[text_id]),
         text_id)
                for text_id in text_id_set]
    weighted_ids.sort(reverse=True)
    
    # Include all ids that are at least as close as a
    # title id, and other ids to a max total of 12
    min_similarity = similarities.similarity(
            concept.vector, finder.index.vectors_dict[
                    concept.title_ids[-1]])
    ref_ids = []
    for weighted_id in weighted_ids:
        if (weighted_id[0] > min_similarity
            ) or len(ref_ids) < 12:
                ref_ids.append(weighted_id[1])
        else: break
        
    concept.references_html = finder.get_refs_html(ref_ids)


class Concept:
    def __init__(self, vector, title, title_vector, summary,
                 cluster, level, title_ids):
        self.concept_ix = None
        self.title = title
        self.summary = summary
        self.vector = vector
        self.title_vector = title_vector
        self.text_id_set = set(cluster)
        self.level = level
        self.title_ids = title_ids
        self.child_ix_set = set()
        self.nominal_size = None
        self.nominal_diff = None
        self.references_html = None
        
        
    def __str__(self):
        return self.title


    def weight(self):
        return len(self.text_id_set)


    def distinct_relevant_ids_match(
            self, distinct_relevant_ids, vectors_dict,
            similarities, focus):
        for i in range(min(3, len(distinct_relevant_ids), 
                           len(self.title_ids))):
            if similarities.similarity(
                        vectors_dict[distinct_relevant_ids[i]],
                        vectors_dict[self.title_ids[i]]
                    ) < focus:
                return False
        return True