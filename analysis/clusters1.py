import time


BASE_DISTANCE = 0.66


class Clusters:
    def __init__(self, ids, vectors_dict, similarities, 
                 reporter):
        self.start_time = time.time()
        self.ids = ids
        self.vectors_dict = vectors_dict
        self.similarities = similarities
        self.reporter = reporter
        self.distances = self.__distances__()
        self.reporter.report('Finding closest columns and row')
        self.closest_cols = self.__closest_cols__()
        self.closest_row = self.__closest_row__()
        self.concept_clusters = None
        self.clusters = [__create_cluster__(
                [vector_id], vectors_dict, similarities) 
                for vector_id in ids]
        self.min_concept_cluster_size = int(len(ids)/100) + 1
        self.__cluster__()
        
        
    def clustering_time(self):
        return  __elapsed_time_str__(self.start_time)
   
    
    def concept_clusters(self): 
        return self.concept_clusters
        
        
    def __distances__(self):
        activity_name  = 'Calculating distances'
        self.reporter.start_progress_reporting(activity_name)
        n = len(self.ids)
        nr_to_do = int((n*(n-1))/2)
        nr_done = 0
        distances = []
        for i in range(n):
            distancesi = []
            for j in range(i):
                distancesi.append((1-self.similarities.similarity(
                    self.vectors_dict[self.ids[i]],
                    self.vectors_dict[self.ids[j]])**2)**0.5)
            nr_done += i
            distances.append(distancesi)
            if i > 1000:
                self.reporter.progress(
                        activity_name, nr_done/nr_to_do)
        self.reporter.cancel_progress_reporting(activity_name)
        return distances
            
            
    def __closest_cols__(self):
        closest_cols = [None]
        for i in range(1,len(self.ids)):
            closest_cols.append(
                self.distances[i].index(
                    min(self.distances[i])))
        return closest_cols
                
                
    def __closest_row__(self):
        if self.closest_cols is None: return None
        min_dist = None
        closest_row = None
        for i in range(1, len(self.closest_cols)):
            dist = self.distances[i][self.closest_cols[i]]
            if min_dist is None or dist < min_dist:
                min_dist = dist
                closest_row = i
        return closest_row
    
        
    def __cluster__(self):
        activity_name = 'Clustering'
        self.reporter.start_progress_reporting(activity_name)
        self.concept_clusters = []
        nr_to_do = len(self.clusters) - 1
        nr_done = 0
        while len(self.clusters) > 1:  
            closest_pair = self.__closest_pair__()
            extended_cluster = self.clusters[
                    closest_pair[0]].copy()            
            discarded_cluster = self.clusters[
                    closest_pair[1]].copy()            
            self.__merge_pair__(closest_pair)
            combined_cluster = self.clusters[closest_pair[0]]
            
            # If both clusters have at least the minimum number
            # of "own" ids, include both as concept clusters. 
            # If one does not, then it is too small, and the 
            # other cluster is either too small or not 
            # significantly different from its parent, so
            # don't include either cluster   
            extended_cluster.set_own_id_set(
                    combined_cluster, self.similarities)
            discarded_cluster.set_own_id_set(
                    combined_cluster, self.similarities)
            if len(extended_cluster.own_id_set
                   ) >= self.min_concept_cluster_size and (
                       len(discarded_cluster.own_id_set
                           ) >= self.min_concept_cluster_size):
                self.concept_clusters.append(extended_cluster)
                self.concept_clusters.append(discarded_cluster)
                    
            nr_done += 1
            self.reporter.progress(
                    activity_name, nr_done/nr_to_do)
        self.reporter.cancel_progress_reporting(activity_name)
        head_cluster = self.clusters[0]
        head_cluster.own_id_set = set(head_cluster.ids)
        self.concept_clusters.append(head_cluster)
        
        
    def __closest_pair__(self):
        return((
            self.closest_row,
            self.closest_cols[self.closest_row]))
        
        
    def __merge_pair__(self, pair):
        if pair is None or self.clusters is None: return
        extended_cluster_ix = pair[0]
        discarded_cluster_ix = pair[1]
        self.__transfer_distances__(
            extended_cluster_ix,
            discarded_cluster_ix)
        self.__update_closest_cols__(
            extended_cluster_ix, discarded_cluster_ix)
        # Closest cols are now as they will be
        # after the discarded row and column are
        # removed
        del self.distances[discarded_cluster_ix]
        for ds in self.distances:
            if len(ds) > discarded_cluster_ix:
                del ds[discarded_cluster_ix]
        self.clusters[extended_cluster_ix].extend(
                self.clusters[discarded_cluster_ix],
                self.vectors_dict, self.similarities)
        del self.clusters[discarded_cluster_ix]
        self.__update_closest_row__()


    def __transfer_distances__(
            self, extended_cluster_ix, 
            discarded_cluster_ix):
        for i in range(len(self.distances)):
            if i > discarded_cluster_ix:
                distance_to_transfer = self.distances[
                        i][discarded_cluster_ix]
            elif i < discarded_cluster_ix:
                distance_to_transfer = self.distances[
                        discarded_cluster_ix][i]
            else: distance_to_transfer = 0
            if i > extended_cluster_ix:
                self.distances[i][extended_cluster_ix
                        ] += distance_to_transfer
            elif i < extended_cluster_ix:
                self.distances[extended_cluster_ix][i
                        ] += distance_to_transfer


    def __update_closest_cols__(
            self, extended_cluster_ix, 
            discarded_cluster_ix):
        # This function is called after distances have 
        # been updated for a merge, but before the row
        # and column for the discarded cluster have been
        # deleted.
        
        # The columns for rows before the row of the 
        # discarded cluster will not change, so their
        # closest columns will not change. The row of the
        # discarded cluster will be deleted, but not until 
        # the updates are complete, as they look up distances
        # assuming that no rows or columns have been deleted.
        # (Note that the extended cluster ix is always greater
        # than the discarded cluster ix)
        if self.closest_cols is None: return
        for i in range(discarded_cluster_ix+1, 
                       len(self.distances)):
            cc = self.closest_cols[i]
            # If the closest column is to be extended or
            # discarded, calculate the new closest column
            # from those that will not be discarded
            if cc == extended_cluster_ix or (
                    cc == discarded_cluster_ix):
                cc = None
                min_avg_dist = None
                for j in range(i):
                    if j != discarded_cluster_ix:
                        avg_dist = self.__avg_dist__(i, j)
                        if cc is None or avg_dist < min_avg_dist:
                            cc = j
                            min_avg_dist = avg_dist
                self.closest_cols[i] = cc
                # This can be None, but only if the discarded
                # column is the only column in the row, which
                # can only happen for row 1 with discarded
                # column 0. Row 0 will then be deleted as the
                # row of the discarded column, so row 1 will 
                # become row 0, which is not used to find the 
                # closest pair.
                
            # Otherwise, the only column that has changed is the
            # column for the extended cluster. There will only be
            # such a column if i > extended_cluster_ix. Change the 
            # closest column if this changed column is closer.
            else:
                if i > extended_cluster_ix:
                    row_closest_avg_dist = self.__avg_dist__(i, cc)
                    extended_avg_dist = self.__avg_dist__(
                            i, extended_cluster_ix)
                    if extended_avg_dist < row_closest_avg_dist or (
                            (extended_avg_dist == row_closest_avg_dist and (
                                extended_cluster_ix < cc))):
                        self.closest_cols[i] = extended_cluster_ix
            # Give the closest col the position that it will have
            # after the discarded column has been deleted.
            cc = self.closest_cols[i]
            if cc is not None and cc > discarded_cluster_ix:
                self.closest_cols[i] -= 1
        # Now remove the discarded row from the list of closest
        # columns
        del self.closest_cols[discarded_cluster_ix]
                    
                    
    def __update_closest_row__(self):
        # This function is called after distances have 
        # been updated for a merge, and after the row
        # and column for the discarded cluster have been
        # deleted.
        if self.closest_cols is None: return
        cr = None
        min_avg_dist = None
        for i in range(1, len(self.closest_cols)):
            if self.closest_cols[i] is not None: 
                avg_dist = self.__avg_dist__(i, self.closest_cols[i])
                if cr is None or avg_dist < min_avg_dist:
                    cr = i
                    min_avg_dist = avg_dist
        self.closest_row = cr
            
            
    def __avg_dist__(self, i, j):
        return self.distances[i][j]/(
                len(self.clusters[i].ids)*len(self.clusters[j].ids))
    
    
class Cluster:
    def __init__(self, ids):
        self.ids = ids
        self.centroid = None
        self.centre_sims_dict = None
        self.own_id_set = None
        
        
    def copy(self):
        cluster = Cluster(self.ids.copy())
        cluster.centroid = self.centroid
        cluster.centre_sims_dict = self.centre_sims_dict.copy()
        if self.own_id_set is None: cluster.own_id_set = None
        else: cluster.own_id_set = self.own_id_set.copy()
        return cluster
        
        
    def extend(self, sibling_cluster, vectors_dict, 
               similarities):
        self.ids.extend(sibling_cluster.ids)
        centroid = similarities.centroid(
            [vectors_dict[vector_id] for vector_id in self.ids])
        self.centroid = centroid
        self.centre_sims_dict = {
            vector_id: similarities.similarity(
                vectors_dict[vector_id], centroid
             ) for vector_id in self.ids}
        self.own_id_set = None
        
        
    def set_own_id_set(self, parent_cluster, similarities):
        self.own_id_set = {
            vector_id for vector_id in self.centre_sims_dict
                if self.centre_sims_dict[vector_id
                        ] > parent_cluster.centre_sims_dict[
                                vector_id]}


def __create_cluster__(ids, vectors_dict, similarities):
    cluster =  Cluster(ids)
    centroid = similarities.centroid(
            [vectors_dict[vector_id] for vector_id in ids])
    cluster.centroid = centroid
    cluster.centre_sims_dict = {
            vector_id: similarities.similarity(
                vectors_dict[vector_id], centroid
             ) for vector_id in ids}
    return cluster


def __elapsed_time_str__(start_time):  
    elapsed_secs = int(time.time() - start_time)
    elapsed_mins = int(elapsed_secs/60)
    elapsed_secs -= elapsed_mins*60
    elapsed_hours = int(elapsed_mins/60)
    elapsed_mins -= elapsed_hours*60
    if elapsed_hours > 0: 
        return str(elapsed_hours
                ) + ' hours, ' + str(elapsed_mins
                ) + ' minutes, ' + str(elapsed_secs
                ) + ' seconds'
    elif elapsed_mins > 0: 
        return str(elapsed_mins
                ) + ' minutes, ' + str(elapsed_secs
                ) + ' seconds'
    else: return str(elapsed_secs
                ) + ' seconds'
      
    
def parent_ix(ix, concept_clusters): 
    elt = concept_clusters[ix].ids[0]
    for i in range(ix+1, len(concept_clusters)):
        if elt in concept_clusters[i].ids: return i
    return None
        
    
def child_ixs(ix, concept_clusters):
    parent_cluster = concept_clusters[ix]
    child_ixs = [] 
    for i in range(ix-1, -1, -1):
        elt_i = concept_clusters[i].ids[0]
        if elt_i in parent_cluster.ids: 
            child = True
            for j in child_ixs:
                if elt_i in concept_clusters[j].ids:
                    child = False
                    break
            if child: child_ixs.append(i)
    return child_ixs
