import radius_clusters


def get_ideas(radius_clusters, ids, texts_dict):
    ideas = []
    for cluster in radius_clusters:
        principal_id = ids[cluster.principal_ix]
        description = texts_dict[principal_id]
        text_ids = [principal_id] + [ids[ix] 
                for ix in cluster.neighbour_ix_set]
        ideas.append(Idea(
                description, cluster.weight, text_ids))
    return ideas


class Idea:
    def __init__(self, description, weight, text_ids):
        self.description = description
        self.weight = weight
        self.text_ids = text_ids
        self.reference_text_ids = None