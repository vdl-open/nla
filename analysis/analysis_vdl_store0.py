import requests
from vdlpy import vdl
from utils import changes
from utils import reports


nlp_metadata_source_nr = -1008
'''
The numeric identifier of the virtual data lake source containing
metadata for natural language processing.
'''


class Store():
    def __init__(self, vdl_client, credential_and_key, 
                 data_source_nr, public):
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
        self.data_source_nr = data_source_nr
        self.new_item_read_level = None
        if public: self.new_item_read_level = vdl.public_level()
    
    
    # Get the set of item ids of subject versions that have
    # concept-map analyses
    def get_cm_asv_id_set(self):
        analysis_x = vdl.Unknown('Analysis')
        parsed_subject_version_x = vdl.Unknown('Parsed Subject Version')
        subject_version_x = vdl.Unknown('Subject Version')
        
        solutions = self.vdl_client.query(
                [(  analysis_x, 
                    vdl.NamedItm(nlp_metadata_source_nr, 'Concept map analysis Is of Subject version'), 
                    subject_version_x)], 
                self.credential_and_key)
        return {solution['Subject Version'].itemid for solution in solutions}
            
        
    def new_concept_map_analysis_item(self, subject_version_item):
        analysis_handle = 'Analysis'
        start_changes = []
        start_changes.append(vdl.CreateItem(
                analysis_handle, 
                self.data_source_nr, 
                self.new_item_read_level))
        start_changes.append(vdl.PutTriple(
                vdl.Unknown(analysis_handle),
                vdl.NamedItm(nlp_metadata_source_nr, 'Concept map analysis Is of Subject version'),
                subject_version_item))
        return self.vdl_client.update(
                start_changes, self.credential_and_key)[analysis_handle]
                
    
    def store_concepts(self, analysis_item, concepts, reporter):   
        change_batches = []
        self.__add_store_concepts_changes__(
                change_batches, analysis_item, concepts)
        activity_name = 'Storing concepts'
        stored_items_dict = changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, reporter
                    ).make_changes(change_batches, 'Storing concepts')
        return {concepts[int(handle)].principal_id: stored_item.itemid 
                for (handle, stored_item) in stored_items_dict.items()}
        
    
    def store_links(
            self, analysis_item, stored_concept_id_by_principal_id_dict, 
            links, reporter):   
        change_batches = []
        self.__add_store_links_changes__(
            change_batches, stored_concept_id_by_principal_id_dict, 
            analysis_item, links)
        activity_name = 'Storing links'
        changes.BatchUpdater(
                self.vdl_client, self.credential_and_key, reporter
                    ).make_changes(change_batches, 'Storing links')


    def get_incomplete_cma_id_set(self):
        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Analysis'),
                vdl.NamedItm(nlp_metadata_source_nr, 'Concept map analysis Is of Subject version'),
                vdl.Unknown('SV'))],
            self.credential_and_key)
        analysis_item_id_set = {solution['Analysis'].itemid
                                for solution in solutions}

        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Analysis'),
                vdl.NamedItm(nlp_metadata_source_nr, 'Concept map analysis Is Complete'),
                True)],
            self.credential_and_key)
        complete_analysis_item_id_set = {solution['Analysis'].itemid
                                for solution in solutions}
        
        return analysis_item_id_set.difference(complete_analysis_item_id_set)
    
    
    def delete_concept_map_analysis(self, analysis_item):
        self.vdl_client.update(        
                self.__get_delete_analysis_changes__(analysis_item),
                self.credential_and_key)
     

    def __add_store_concepts_changes__(
            self, change_batches, analysis_item, concepts):
        i = 0
        for concept in concepts:
            handle = str(i)
            changes = []
            changes.append(vdl.CreateItem(
                    handle, 
                    self.data_source_nr, 
                    self.new_item_read_level))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(nlp_metadata_source_nr, 'Concept Is in Analysis'),
                    analysis_item))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(nlp_metadata_source_nr, 'Concept Has Title'),
                    concept.title))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(nlp_metadata_source_nr, 'Concept Has Description'),
                    concept.description))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(nlp_metadata_source_nr, 'Concept Has Weight'),
                    float(concept.weight)))
            for (text_id) in concept.reference_text_ids():
                changes.append(vdl.PutTriple(
                        vdl.Unknown(handle),
                        vdl.NamedItm(nlp_metadata_source_nr, 'Concept Has Reference text'),
                        vdl.Itm(text_id)))
            change_batches.append(changes)
            i += 1


    def __add_store_links_changes__(
            self, change_batches, stored_concept_id_by_principal_id_dict, 
            analysis_item, links):
        if links is None: return
        i = 0
        for link in links:
            handle = str(i)
            changes = []
            changes.append(vdl.CreateItem(
                    handle, 
                    self.data_source_nr, 
                    self.new_item_read_level))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(nlp_metadata_source_nr, 'Link Is in Analysis'),
                    analysis_item))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(nlp_metadata_source_nr, 'Link Links Concept'),
                    vdl.Itm(stored_concept_id_by_principal_id_dict[
                        link.concept0_principal_id])))
            changes.append(vdl.PutTriple(
                    vdl.Unknown(handle),
                    vdl.NamedItm(nlp_metadata_source_nr, 'Link Links Concept'),
                    vdl.Itm(stored_concept_id_by_principal_id_dict[
                        link.concept1_principal_id])))
            for text_id in link.reference_text_ids():
                changes.append(vdl.PutTriple(
                        vdl.Unknown(handle),
                        vdl.NamedItm(nlp_metadata_source_nr, 'Link Has Reference text'),
                        vdl.Itm(text_id)))
            change_batches.append(changes)
            i += 1
        

    def __get_delete_analysis_changes__(self, analysis_item):
        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Concept'),
                vdl.NamedItm(nlp_metadata_source_nr, 'Concept Is in Analysis'),
                analysis_item)],
            self.credential_and_key)
        deletion_changes = [vdl.DeleteItem(solution['Concept'])
                             for solution in solutions]

        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Link'),
                vdl.NamedItm(nlp_metadata_source_nr, 'Link Is in Analysis'),
                analysis_item)],
            self.credential_and_key)
        deletion_changes.extend([vdl.DeleteItem(solution['Link'])
                             for solution in solutions])
        
        deletion_changes.append(vdl.DeleteItem(analysis_item))        
        return deletion_changes
