def get_index(parsed_file_set, encoder, reporter):
    activity_name = 'Finding texts'
    texts_dict = {}
    for (section_id, section
         ) in parsed_file_set.sections_dict.items():
        texts_dict[section_id] = section.title
    for (sentence_id, sentence
         ) in parsed_file_set.sentences_dict.items():
        texts_dict[sentence_id] = sentence.text
            
    activity_name = 'Finding vectors'
    vectors_dict = {}
    m_steps = int(len(texts_dict)/1000)
    step = 0
    m_step = 0
    reporter.start_progress_reporting(activity_name)
    for (text_id, text) in texts_dict.items():
        vectors_dict[text_id] = encoder.encode(text)
        step += 1
        if step >= 1000:
            m_step += 1                
            step = 0
            reporter.progress(activity_name, m_step/m_steps)
    reporter.cancel_progress_reporting(activity_name)
        
    return Index(
            parsed_file_set.parsed_files_dict,
            parsed_file_set.sections_dict,
            parsed_file_set.sentences_dict,
            texts_dict, 
            vectors_dict)

        
def get_parsed_file_by_section_dict(index): 
    parsed_file_by_section_dict = {}    
    for (parsed_file_id, parsed_file
         ) in index.parsed_files_dict.items():
        for section_id in parsed_file.section_id_set:
            text = index.texts_dict[section_id]
            parsed_file_by_section_dict[section_id
                    ] = parsed_file_id
    return parsed_file_by_section_dict
 
    
def get_section_by_sentence_dict(index): 
    section_by_sentence_dict = {}    
    for (section_id, section
         ) in index.sections_dict.items():
        for sentence_id in section.sentence_id_set:
            text = index.texts_dict[sentence_id]
            section_by_sentence_dict[sentence_id] = section_id
    return section_by_sentence_dict


class Index:
    def __init__(self, parsed_files_dict,
                 sections_dict, sentences_dict,
                 texts_dict, vectors_dict):
        self.parsed_files_dict = parsed_files_dict
        self.sections_dict = sections_dict
        self.sentences_dict = sentences_dict
        self.texts_dict = texts_dict
        self.vectors_dict = vectors_dict
                