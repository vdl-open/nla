import encoders
import similarities as sims
import indexes
import index_vdl_store
from vdlpy import vdl


class Requester:
    def __init__(self, model_path, vdl_client):
        self.encoder = encoders.Encoder(model_path, True)
        self.similarities = sims.Similarities(True)
        self.vdl_client = vdl_client
        self.index_store = index_vdl_store.Store(
                vdl_client, None)
        self.file_set_attributes_dict = {}
        
        
    def request(self, index_item, key_phrase,
                query_item, result_verb,
                threshold = 0.5, limit = 64):
        attributes = self.__get_attributes__(index_item)
        if attributes is None: return None
        key_vector = self.encoder.encode(key_phrase)
        weighted_ids = self.__get_weighted_ids__(
                key_vector, attributes, threshold, limit)
        matches = []
        for weighted_id in weighted_ids:
            match = self.__match__(weighted_id, attributes)
            if match is not None: 
                matches.append(match.__str__())
        return '\n'.join(matches)
                
                
    def __get_attributes__(self, index_item):
        attributes = self.file_set_attributes_dict.get(
                index_item.itemid)
        if attributes is None:
            index = self.index_store.load_index_from_item(
                    index_item)
            if index is None: return None
            attributes = Attributes(index)
            self.file_set_attributes_dict[index_item.itemid
                    ] = attributes
        return attributes
        
            
    def __get_weighted_ids__(
            self, key_vector, attributes, threshold, limit):       
        weighted_ids = []
        for (text_id, vector
             ) in attributes.vectors_dict.items():
            sim = self.similarities.similarity(
                    key_vector, vector)
            if sim >= threshold:
                weighted_ids.append((sim, text_id))
        weighted_ids.sort(reverse=True)
        if len(weighted_ids) > limit:
            return weighted_ids[:limit]
        else: return weighted_ids
        
        
    def __match__(self, weighted_id, attributes):
        weight = weighted_id[0]
        text_id = weighted_id[1]
        section = attributes.sections_dict.get(text_id)
        if section is None:
            sentence = attributes.sentences_dict.get(text_id)
            if sentence is None: return None
            sentence_text = sentence.text
            section_id = (
                attributes.section_by_sentence_dict.get(
                    text_id))
            if section_id is None: section_header = ''
            else: section_header = attributes.texts_dict[
                    section_id]
        else:
            section_id = text_id
            section_header = section.title
            sentence_text = ''
        parsed_file_id = (
            attributes.parsed_file_by_section_dict[
                section_id])
        parsed_file = attributes.parsed_files_dict[
                parsed_file_id]
        document_title = parsed_file.title
        return Match(document_title, section_header, 
                     sentence_text, weight)    

        
class Match:
    def __init__(self, document_title, section_heading, 
                 sentence_text, weight):
        self.document_title = document_title
        self.section_heading = section_heading
        self.sentence_text = sentence_text
        self.weight = weight
        
        
    def __str__(self):
        return str(self.document_title
            ) + '\t' + str(self.section_heading
            ) + '\t' + str(self.sentence_text)
        
        
class Attributes:
    def __init__(self, index):
        self.parsed_files_dict = index.parsed_files_dict
        self.sections_dict = index.sections_dict
        self.sentences_dict = index.sentences_dict
        self.texts_dict = index.texts_dict
        self.vectors_dict = index.vectors_dict
        self.parsed_file_by_section_dict = (
            indexes.get_parsed_file_by_section_dict(index))
        self.section_by_sentence_dict = (
            indexes.get_section_by_sentence_dict(index))
        
        

