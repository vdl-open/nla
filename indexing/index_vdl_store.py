from vdlpy import vdl
from utils import reports
import pickle
import io


NLP_METADATA_SOURCE_NR = -1008
DUMP_SEGMENT_LENGTH = 1000000


class Store:
    def __init__(self, vdl_client, credential_and_key):
        self.vdl_client = vdl_client
        self.credential_and_key = credential_and_key
        
        
    def get_index_to_delete_id_set(self):
        index_x = vdl.Unknown('Index')
        file_set_x = vdl.Unknown('File Set')                
        solutions = self.vdl_client.query(
                [(  index_x,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Index Is of File set'),
                    file_set_x)],
                self.credential_and_key)
        index_id_set_dict = {}
        for solution in solutions:
            file_set_id = solution['File Set'].itemid
            index_id = solution['Index'].itemid
            index_id_set = index_id_set_dict.get(file_set_id)
            if index_id_set is None:
                index_id_set_dict[file_set_id] = {index_id}
            else: index_id_set.add(index_id)

        solutions = self.vdl_client.query(
                [(  index_x,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Index Is Complete'),
                    True)],
                self.credential_and_key)
        complete_index_id_set = {solution['Index'].itemid
                                 for solution in solutions}
        
        index_to_delete_id_set = set()
        index_to_keep_id_set = set()        
        for (file_set_id, index_id_set) in index_id_set_dict.items():
            file_set_index_id = None
            for index_id in index_id_set:
                if file_set_index_id is None and (
                        index_id in complete_index_id_set):
                    index_to_keep_id_set.add(index_id)
                    file_set_index_id = index_id
                else: index_to_delete_id_set.add(index_id)
        index_to_delete_id_set.update(
            complete_index_id_set.difference(
                index_to_keep_id_set))
        return index_to_delete_id_set
                    
    
    def get_indexed_file_set_id_set(self):
        index_x = vdl.Unknown('Index')
        file_set_x = vdl.Unknown('File Set')                
        solutions = self.vdl_client.query(
                [(  index_x,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Index Is of File set'),
                    file_set_x)],
                self.credential_and_key)
        return {solution['File Set'].itemid 
                for solution in solutions}
        
    
    def get_completely_indexed_file_set_id_set(self):
        index_x = vdl.Unknown('Index')
        file_set_x = vdl.Unknown('File Set')                
        solutions = self.vdl_client.query(
                [(  index_x, 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Index Is Complete'), 
                    True),
                 (  index_x,
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Index Is of File set'),
                    file_set_x)],
                self.credential_and_key)
        return {solution['File Set'].itemid 
                for solution in solutions}
        
        
    def new_index(
            self, file_set_item, source_nr, read_level):
        handle = 'Index'
        items_dict = self.vdl_client.update(
            [ vdl.CreateItem(handle,
                             source_nr,
                             read_level),
              vdl.PutTriple( vdl.Unknown(handle),
                             vdl.NamedItm(
                                NLP_METADATA_SOURCE_NR,
                                'Index Is of File set'),
                             file_set_item)
            ], self.credential_and_key)
        return items_dict[handle]

    
    def load_index(self, file_set_item):
        solutions = self.vdl_client.query(
            [(  vdl.Unknown('Index'), 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Index Is of File set'), 
                file_set_item)],
            self.credential_and_key)
        if len(solutions) == 0: return None
        if len(solutions) > 1: raise Exception(
                'File set has multiple indexes')
        index_item = solutions[0]['Index']
        return self.load_index_from_item(index_item)
        
    
    def load_index_from_item(self, index_item):
        segment_x = vdl.Unknown('Segment')
        solutions = self.vdl_client.query(
            [(  index_item, 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Index Has Segment'), 
                segment_x),
             (  segment_x, 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Index segment Has Sequence nr'), 
                vdl.Unknown('Seq'))],
            self.credential_and_key)
        if len(solutions) == 0: return None
        numbered_segments = [(solution['Seq'], solution['Segment']) 
                             for solution in solutions]
        
        analysis_dump = None
        numbered_segments.sort(key=__elt0__)
        next_seq = 0
        for numbered_segment in numbered_segments:
            if numbered_segment[0] != next_seq:
                raise Exception('Missing segment: ' + str(next_seq))
            next_seq += 1
            solutions = self.vdl_client.query(
                [(  numbered_segment[1], 
                    vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                 'Index segment Has Bytes'), 
                    vdl.Unknown('Bytes'))],
                self.credential_and_key)
            if len(solutions) == 0:
                raise Exception(
                    'No segment with sequence number ' + str(
                        numbered_segment[0]))
            elif len(solutions) > 1:
                raise Exception(
                    'Multiple segments with sequence number ' + str(
                        numbered_segment[0]))
            elif analysis_dump == None: 
                analysis_dump = solutions[0]['Bytes']
            else: analysis_dump += solutions[0]['Bytes']            
        return pickle.loads(analysis_dump)

        
    def store_index(
            self, index_item, index, source_nr, 
            read_level, reporter):
        dump_bytes = None
        with io.BytesIO() as b:
            pickle.dump(index, b)
            dump_bytes = b.getvalue()
        
        action_name = 'Storing index'
        reporter.start_progress_reporting(action_name)
        nr_of_segments = 0;
        dump_length = 0
        i = 0
        while dump_length < len(dump_bytes):
            changes = []
            segment_handle = 'S' + str(i)
            changes.append(vdl.CreateItem(
                segment_handle,
                source_nr,
                read_level))
            changes.append(vdl.PutTriple(
                index_item,
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Index Has Segment'),
                vdl.Unknown(segment_handle))),
            changes.append(vdl.PutTriple(
                vdl.Unknown(segment_handle),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Index segment Has Sequence nr'),
                i))
            if dump_length + DUMP_SEGMENT_LENGTH < len(dump_bytes):
                segment_end = dump_length + DUMP_SEGMENT_LENGTH
            else: segment_end = len(dump_bytes)
            changes.append(vdl.PutTriple(
                vdl.Unknown(segment_handle),
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                             'Index segment Has Bytes'),
                dump_bytes[dump_length:segment_end]))
            self.vdl_client.update(
                    changes, self.credential_and_key)
            reporter.progress(
                    action_name, dump_length/len(dump_bytes))
            dump_length = segment_end
            i += 1
        reporter.cancel_progress_reporting(action_name)
            

    def note_complete(self, index_item):
        self.vdl_client.update(
            [vdl.SetUniqueObject(
                index_item,
                vdl.NamedItm(NLP_METADATA_SOURCE_NR,
                             'Index Is Complete'),
                True)],
            self.credential_and_key)        
        
        
    def delete_stored_index(self, index_item):
        solutions = self.vdl_client.query(
            [(  index_item, 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                'Index Has Segment'), 
                vdl.Unknown('Segment'))], 
            self.credential_and_key)  
        changes = [vdl.DeleteItem(solution['Segment'])
                   for solution in solutions]      
        changes.append(vdl.DeleteItem(index_item))
        self.vdl_client.update(changes, self.credential_and_key)
        
        
    def delete_orphaned_segments(self):
        index_solutions = self.vdl_client.query(
            [(  vdl.Unknown('Index'), 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                'Index Has Segment'), 
                vdl.Unknown('Segment'))], 
            self.credential_and_key) 
        index_segment_ids = {
            index_solution['Segment'].itemid
            for index_solution in index_solutions}
        segment_solutions = self.vdl_client.query(
            [(  vdl.Unknown('Segment'), 
                vdl.NamedItm(NLP_METADATA_SOURCE_NR, 
                                'Index segment Has Sequence nr'), 
                vdl.Unknown('Seq'))], 
            self.credential_and_key)  
        segment_ids = {
                segment_solution['Segment'].itemid
                for segment_solution in segment_solutions}
        orphaned_segment_ids = segment_ids.difference(
                index_segment_ids)
        changes = [vdl.DeleteItem(vdl.Itm(orphaned_segment_id))
                   for orphaned_segment_id in orphaned_segment_ids]      
        self.vdl_client.update(changes, self.credential_and_key)
        

def __elt0__(tup):
    return tup[0]
